#pragma once

#include <array>
#include <vector>
#include <memory>
#include <iostream>
#include <chrono>
#include <thread>

#include "Constants.h"
#include "Client.h"

typedef std::array<short, Constants::BOARD_SIZE> Board;

class Pexeso
{
    Board board;
    void generateBoard();
    int playerTurnIndex = 0;
    int cardRevealedInTurn = 0;
    int firstRevealedCardId;
    std::vector<ClientPointer> players;
    void (*onGameEnd)(int roomId);
    void (*onNextTurn)(int roomId, ClientPointer client);
    bool playerScored = true;
    int scoreAchieved = 0;
    void roundTimer();
    int roomId;
    bool lockedTurn = false;

public:
    Pexeso(int roomId, std::vector<ClientPointer> &players, void (*onGameEnd)(int roomId), void (*onNextTurn)(int roomId, ClientPointer client));
    Board &getBoard();
    bool revealCard(int id, ClientPointer client);
    void start();
    std::vector<ClientPointer> &getPlayers();
    ClientPointer getPlayerOnTurn();
    void lockTurn();
};

typedef std::unique_ptr<Pexeso> GamePointer;