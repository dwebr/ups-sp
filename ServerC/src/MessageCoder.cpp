
#include <iomanip>
#include <iostream>

#include "MessageCoder.h"

int decodeInt(std::string message, int *number)
{
    try
    {
        *number = std::stoi(message);
    }
    catch (...)
    {
        return 1;
    }
    return 0;
}

int decodeString(std::string message, std::string *string, int prefixLength)
{
    try
    {
        if (std::stoi(message.substr(0, prefixLength)) != message.length() - prefixLength)
        {
            return 2;
        }
    }
    catch (...)
    {
        return 1;
    }
    *string = message.substr(prefixLength);

    return 0;
}

int decodeHeader(std::string message, std::string *protocol_identification, ClientMessageType *messageType, int *messageLength)
{
    *protocol_identification = message.substr(0, Constants::MESSAGE_PROTOCOL_IDENTIFICATOR.length());
    message = message.substr(Constants::MESSAGE_PROTOCOL_IDENTIFICATOR.length());

    *messageType = stringToClientMessageType(message.substr(0, Constants::MESSAGE_TYPE_LENGTH));
    message = message.substr(Constants::MESSAGE_TYPE_LENGTH);

    try
    {
        *messageLength = std::stoi(message);
    }
    catch (...)
    {
        return 1;
    }

    return 0;
}

std::string encapsulateMessage(std::string messageBody, ServerMessageType messageType)
{
    std::ostringstream message;
    message << Constants::MESSAGE_PROTOCOL_IDENTIFICATOR << serverMessageTypeToString(messageType);
    message << std::setfill('0') << std::setw(Constants::MESSAGE_LENGTH_DECIMALS) << messageBody.length();
    message << messageBody << std::endl;
    return message.str();
}

std::string encodeNumber(int number, int length)
{
    std::ostringstream encodedNumber;
    encodedNumber << std::setfill('0') << std::setw(length) << number;
    return encodedNumber.str();
}

std::string encodeResponse(int responseCode, std::string message)
{
    std::ostringstream messageBody;
    messageBody << std::setfill('0') << std::setw(Constants::MESSAGE_NICK_DECIMALS) << responseCode;
    messageBody << std::setfill('0') << std::setw(Constants::MESSAGE_RESPONSE_MESSAGE_DECIMALS) << message.length();
    messageBody << message;
    return encapsulateMessage(messageBody.str(), ServerMessageType::RESPONSE);
}

std::string encodeRoomList(std::map<int, RoomPointer> &rooms)
{
    std::ostringstream encodedRoomsList;
    for (auto &roomPair : rooms)
    {
        int roomId = roomPair.first;
        Room &room = *roomPair.second;

        if (room.isInGame())
        {
            continue;
        }

        std::ostringstream encodedRoom;
        encodedRoom << std::setfill('0') << std::setw(Constants::MESSAGE_ROOM_ID_DECIMALS) << roomId;
        encodedRoom << std::setfill('0') << std::setw(Constants::MESSAGE_NICK_DECIMALS) << room.getOwner().size();
        encodedRoom << room.getOwner();
        std::vector<ClientPointer> players = room.getPlayers();
        std::ostringstream encodedPlayersList;
        for (ClientPointer player : players)
        {
            encodedPlayersList << std::setfill('0') << std::setw(Constants::MESSAGE_NICK_DECIMALS) << player->getNickName().size();
            encodedPlayersList << player->getNickName();
        }
        encodedRoom << std::setfill('0') << std::setw(Constants::MESSAGE_PLAYERS_LIST_DECIMALS) << encodedPlayersList.str().length();
        encodedRoom << encodedPlayersList.str();
        encodedRoomsList << encodedRoom.str();
    }
    std::ostringstream messageBody;
    messageBody << std::setfill('0') << std::setw(Constants::MESSAGE_ROOM_LIST_DECIMALS) << encodedRoomsList.str().length();
    messageBody << encodedRoomsList.str();
    return encapsulateMessage(messageBody.str(), ServerMessageType::ROOM_LIST_MESSAGE);
}

std::string encodeStartGame(Board board)
{
    std::ostringstream encodedBoard;
    for (auto cardType : board)
    {
        encodedBoard << std::setfill('0') << std::setw(Constants::MESSAGE_CARD_TYPE_DECIMALS) << cardType;
    }

    return encapsulateMessage(encodedBoard.str(), ServerMessageType::GAME_START_MESSAGE);
}

std::string encodeRevealCard(int cardId)
{
    std::ostringstream encodedCard;
    encodedCard << std::setfill('0') << std::setw(Constants::MESSAGE_CARD_ID_DECIMALS) << cardId;
    return encapsulateMessage(encodedCard.str(), ServerMessageType::REVEAL_CARD_MESSAGE);
}

std::string encodePlayersTurn(std::string nick)
{
    std::ostringstream encodedNick;
    encodedNick << std::setfill('0') << std::setw(Constants::MESSAGE_NICK_DECIMALS) << nick.length();
    encodedNick << nick;
    return encapsulateMessage(encodedNick.str(), ServerMessageType::PLAYERS_TURN_MESSAGE);
}

std::string encodeGameEnd()
{
    return encapsulateMessage("", ServerMessageType::GAME_END_MESSAGE);
}

std::string encodePlayersConnection(std::string nick, ClientConnectionStatus connection)
{
    std::ostringstream encodedConnection;
    encodedConnection << std::setfill('0') << std::setw(Constants::MESSAGE_CONNECTION_DECIMALS) << clientConnectionStatusToInt(connection);
    encodedConnection << std::setfill('0') << std::setw(Constants::MESSAGE_NICK_DECIMALS) << nick.length();
    encodedConnection << nick;
    return encapsulateMessage(encodedConnection.str(), ServerMessageType::PLAYERS_STATUS_MESSAGE);
}

std::string encodeGameStatus(Pexeso &game)
{
    std::ostringstream encodedGameStatus;
    for (auto cardType : game.getBoard())
    {
        encodedGameStatus << std::setfill('0') << std::setw(Constants::MESSAGE_CARD_TYPE_DECIMALS) << cardType;
    }
    encodedGameStatus << std::setfill('0') << std::setw(Constants::MESSAGE_NICK_DECIMALS) << game.getPlayerOnTurn()->getNickName().size();
    encodedGameStatus << game.getPlayerOnTurn()->getNickName();
    std::ostringstream encodedPlayersStatus;
    for (ClientPointer player : game.getPlayers())
    {
        encodedPlayersStatus << std::setfill('0') << std::setw(Constants::MESSAGE_NICK_DECIMALS) << player->getNickName().size();
        encodedPlayersStatus << player->getNickName();
        encodedPlayersStatus << std::setfill('0') << std::setw(Constants::MESSAGE_CONNECTION_DECIMALS) << clientConnectionStatusToInt(player->getConnectionStatus());
        encodedPlayersStatus << std::setfill('0') << std::setw(Constants::MESSAGE_SCORE_DECIMALS) << player->getScore();
    }

    encodedGameStatus << std::setfill('0') << std::setw(Constants::MESSAGE_PLAYERS_STATUS_LIST_DECIMALS) << encodedPlayersStatus.str().length();
    encodedGameStatus << encodedPlayersStatus.str();

    return encapsulateMessage(encodedGameStatus.str(), ServerMessageType::GAME_STATUS_MESSAGE);
}

ClientMessageType stringToClientMessageType(std::string type)
{
    if (type == Constants::REQUEST_LOGIN)
        return ClientMessageType::LOGIN;

    else if (type == Constants::REQUEST_REVEAL_CARD)
        return ClientMessageType::REVEAL_CARD;

    else if (type == Constants::REQUEST_ROOM_CREATE)
        return ClientMessageType::ROOM_CREATE;

    else if (type == Constants::REQUEST_ROOM_DELETE)
        return ClientMessageType::ROOM_DELETE;

    else if (type == Constants::REQUEST_ROOM_ENTER)
        return ClientMessageType::ROOM_ENTER;

    else if (type == Constants::REQUEST_ROOM_LEAVE)
        return ClientMessageType::ROOM_LEAVE;

    else if (type == Constants::REQUEST_ROOM_START)
        return ClientMessageType::ROOM_START;

    else if (type == Constants::PING)
        return ClientMessageType::PING;

    else
        return ClientMessageType::UNKNOWN;
}

std::string serverMessageTypeToString(ServerMessageType messageType)
{
    switch (messageType)
    {

    case ServerMessageType::RESPONSE:
        return Constants::SERVER_RESPONSE;

    case ServerMessageType::GAME_END_MESSAGE:
        return Constants::SERVER_GAME_END_MESSAGE;

    case ServerMessageType::GAME_START_MESSAGE:
        return Constants::SERVER_GAME_START_MESSAGE;

    case ServerMessageType::GAME_STATUS_MESSAGE:
        return Constants::SERVER_GAME_STATUS_MESSAGE;

    case ServerMessageType::PLAYERS_STATUS_MESSAGE:
        return Constants::SERVER_PLAYERS_STATUS_MESSAGE;

    case ServerMessageType::PLAYERS_TURN_MESSAGE:
        return Constants::SERVER_PLAYERS_TURN_MESSAGE;

    case ServerMessageType::REVEAL_CARD_MESSAGE:
        return Constants::SERVER_REVEAL_CARD_MESSAGE;

    case ServerMessageType::ROOM_LIST_MESSAGE:
        return Constants::SERVER_ROOM_LIST_MESSAGE;
    }
    return "1";
}

int clientConnectionStatusToInt(ClientConnectionStatus status)
{
    switch (status)
    {
    case ClientConnectionStatus::CONNECTED:
        return Constants::MESSAGE_CONNECTED;

    case ClientConnectionStatus::DISCONNECTED:
        return Constants::MESSAGE_DISCONNECTED;

    case ClientConnectionStatus::PENDING:
        return Constants::MESSAGE_PENDING;
    }

    return -1;
}