#pragma once

#include <iomanip>
#include <iostream>
#include <map>
#include <memory>

#include "Constants.h"
#include "MessageType.h"
#include "Room.h"
#include "Pexeso.h"

int decodeInt(std::string message, int *number);
int decodeString(std::string message, std::string *string, int prefixLength);
int decodeHeader(std::string message, std::string *protocol_identification, ClientMessageType *messageType, int *messageLength);
std::string encapsulateMessage(std::string messageBody, ServerMessageType messageType);
std::string encodeNumber(int number, int length);
std::string encodeResponse(int responseCode, std::string message);
std::string encodeRoomList(std::map<int, RoomPointer> &rooms);
std::string encodeStartGame(Board board);
std::string encodeRevealCard(int cardId);
std::string encodePlayersTurn(std::string nick);
std::string encodeGameEnd();
std::string encodePlayersConnection(std::string nick, ClientConnectionStatus connection);
std::string encodeGameStatus(Pexeso &game);
ClientMessageType stringToClientMessageType(std::string type);
std::string serverMessageTypeToString(ServerMessageType messageType);
int clientConnectionStatusToInt(ClientConnectionStatus status);