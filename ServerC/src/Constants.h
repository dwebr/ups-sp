#pragma once

#include "ConfigurationLoader.h"

namespace Constants
{
    static std::string const CONFIGURATION_FILE = "serverConfiguration.txt";
    static std::string const MESSAGE_PROTOCOL_IDENTIFICATOR = "KIVUPS";

    static std::string const REQUEST_LOGIN = "lgin";
    static std::string const REQUEST_REVEAL_CARD = "rvcc";
    static std::string const REQUEST_ROOM_CREATE = "rmcr";
    static std::string const REQUEST_ROOM_DELETE = "rmdl";
    static std::string const REQUEST_ROOM_ENTER = "rmen";
    static std::string const REQUEST_ROOM_LEAVE = "rmlv";
    static std::string const REQUEST_ROOM_START = "rmst";
    static std::string const PING = "ping";

    static std::string const SERVER_RESPONSE = "resp";
    static std::string const SERVER_ROOM_LIST_MESSAGE = "list";
    static std::string const SERVER_REVEAL_CARD_MESSAGE = "rvcs";
    static std::string const SERVER_PLAYERS_TURN_MESSAGE = "ptrn";
    static std::string const SERVER_PLAYERS_STATUS_MESSAGE = "psts";
    static std::string const SERVER_GAME_END_MESSAGE = "gend";
    static std::string const SERVER_GAME_START_MESSAGE = "gstr";
    static std::string const SERVER_GAME_STATUS_MESSAGE = "gsts";

    static int const MESSAGE_TYPE_LENGTH = 4;
    static int const MESSAGE_LENGTH_DECIMALS = 4;
    static int const MESSAGE_RESPONSE_CODE_DECIMALS = 2;
    static int const MESSAGE_RESPONSE_MESSAGE_DECIMALS = 2;
    static int const MESSAGE_ROOM_LIST_DECIMALS = 4;
    static int const MESSAGE_PLAYERS_LIST_DECIMALS = 3;
    static int const MESSAGE_PLAYERS_STATUS_LIST_DECIMALS = 3;
    static int const MESSAGE_CARD_ID_DECIMALS = 2;
    static int const MESSAGE_ROOM_ID_DECIMALS = 2;
    static int const MESSAGE_NICK_DECIMALS = 2;
    static int const MESSAGE_CARD_TYPE_DECIMALS = 2;
    static int const MESSAGE_CONNECTION_DECIMALS = 1;
    static int const MESSAGE_SCORE_DECIMALS = 2;
    static int const MESSAGE_PING_TYPE_DECIMALS = 1;

    static int const RESPONSE_SUCCESS = 1;
    static int const RESPONSE_FAIL = 2;

    static int const PING_STANDARD = 1;
    static int const PING_LOST_CONNECTION = 2;

    static int const MESSAGE_CONNECTED = 1;
    static int const MESSAGE_DISCONNECTED = 2;
    static int const MESSAGE_PENDING = 3;

    static int const BOARD_SIZE = 32;
    static int const ROUND_TIMER = 8;
    static int const GAME_MAX_NUMBER_OF_PLAYERS = 10;
    static int const GAME_MIN_NUMBER_OF_PLAYERS = 2;

    static int const MINIMAL_NICK_LENGTH = 4;
    static int const MAXIMAL_NICK_LENGTH = 12;

    static int const MAX_MESSAGE_SIZE = 4096;
    static int const TIMEOUT_DISCONECT_INTERVAL = 20;
    static int const TIMEOUT_PENDING_INTERVAL = 6;

    // configurable with file
    static std::string const SERVER_ADDRESS = "127.0.0.1";
    static int const SERVER_PORT = 10000;
    static int const MAX_CLIENTS_QUEUE = 5;
    static int const MAX_CLIENTS = 30;
    static int const MAX_ROOM_COUNT = 15;

} // namespace Constants
