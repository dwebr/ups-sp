
#pragma once

#include <sys/un.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <time.h>
#include <errno.h>
#include <arpa/inet.h>
#include <vector>
#include <iostream>
#include <map>
#include <array>
#include <sstream>
#include <numeric>

#include "Client.h"
#include "Constants.h"
#include "MessageType.h"
#include "Constants.h"
#include "MessageCoder.h"

typedef void (*OnMessageReceivedFunction)(std::string messageBody, ClientPointer);
typedef void (*OnPlayerConnectionChangeFunction)(ClientConnectionStatus status, ClientPointer client);

class Network
{
    ConfigurationLoader cl;

    std::map<int, ClientPointer> clients;
    fd_set currentSockets, readySockets;
    int serverSocket;
    std::vector<char> buffer;
    int maxSocket;
    int a2read;
    int headerLength;

    OnMessageReceivedFunction onMessageReceived;
    OnPlayerConnectionChangeFunction onPlayerConnectionChange;

    sockaddr_in inaddr;
    socklen_t addrlen = sizeof(sockaddr_in);

    void handleNewClient();
    void handleNewMessage(int clientSocketId);
    int readHeader(std::string &message, ClientPointer client);
    void clearClient(ClientPointer client, std::string message);

public:
    Network(ConfigurationLoader cl, OnMessageReceivedFunction onMessageReceived, OnPlayerConnectionChangeFunction onPlayerConnectionChange);
    std::map<int, ClientPointer> &getClients();
    int init();
    void sendMessageToNonPlayingClients(std::string &body, Client &clientToSkip);
    void sendMessageToClient(std::string &body, Client &client, bool log = true);
    void receiveMessages();
};