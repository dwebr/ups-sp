#pragma once
enum class ClientMessageType
{
    PING,
    LOGIN,
    REVEAL_CARD,
    ROOM_CREATE,
    ROOM_DELETE,
    ROOM_ENTER,
    ROOM_LEAVE,
    ROOM_START,
    RESPONSE,
    UNKNOWN
};

enum class ServerMessageType
{
    GAME_END_MESSAGE,
    GAME_START_MESSAGE,
    GAME_STATUS_MESSAGE,
    PLAYERS_STATUS_MESSAGE,
    PLAYERS_TURN_MESSAGE,
    REVEAL_CARD_MESSAGE,
    ROOM_LIST_MESSAGE,
    RESPONSE,
    UNKNOWN
};