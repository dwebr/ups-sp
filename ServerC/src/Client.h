#pragma once
#include <iostream>
#include <array>
#include <memory>
#include <algorithm>
#include <iterator>

#include "MessageType.h"
#include "Constants.h"

enum class ClientState
{
    LOGIN,
    LOBBY,
    ROOM,
    PLAYING,
    PLAYING_WAITING
};

enum class ClientConnectionStatus
{
    CONNECTED,
    DISCONNECTED,
    PENDING
};

class Client
{
    int socketId;
    std::array<char, Constants::MAX_MESSAGE_SIZE> buffer;
    int bufferIndex = 0;
    int expectedMessageLength = -1;
    ClientMessageType receivingMessage = ClientMessageType::UNKNOWN;
    time_t lastActive = time(NULL);
    ClientState state = ClientState::LOGIN;
    ClientConnectionStatus connectionStatus = ClientConnectionStatus::CONNECTED;
    std::string nickname = "";
    int score = 0;
    int roomId = -1;
    bool sentStatusMessage = false;

public:
    Client(int socketId);
    void resetBuffer();
    void copyNetworkInfo(Client client);
    int getSocketId();
    void setSocketId(int socketId);
    std::string getNickName();
    void setNickName(std::string nickname);
    int getScore();
    void increaseScore();
    void resetScore();
    ClientConnectionStatus getConnectionStatus();
    void setConnectionStatus(ClientConnectionStatus connectionStatus);
    int getBufferIndex();
    void increaseBufferIndex(int value);
    std::array<char, Constants::MAX_MESSAGE_SIZE> &getBuffer();
    int getExpectedMessageLength();
    void setExpectedMessageLength(int length);
    time_t getLastActive();
    void setLastActive(time_t time);
    ClientState getClientState();
    void setClientState(ClientState state);
    void setReceivingMessageType(ClientMessageType receivingMessage);
    ClientMessageType getReceivingMessageType();
    void setRoomId(int id);
    int getRoomId();
    void setSendStatusMessage(bool sent);
    bool wasStatusMessageSend();
};

typedef std::shared_ptr<Client> ClientPointer;
