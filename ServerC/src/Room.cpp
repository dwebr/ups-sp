#include "Room.h"

Room::Room(int id, std::string owner) : id(id), owner(owner){};

std::string Room::getOwner()
{
    return owner;
};

int Room::addPlayer(ClientPointer client)
{
    if (players.size() >= Constants::GAME_MAX_NUMBER_OF_PLAYERS)
    {
        return 1;
    }

    players.push_back(client);
    return 0;
};

int Room::removePlayer(ClientPointer client)
{
    players.erase(std::remove(players.begin(), players.end(), client), players.end());
    return 0;
};

std::vector<ClientPointer> &Room::getPlayers()
{
    return players;
};

Pexeso &Room::newGame(void (*onGameEnd)(int roomId), void (*onNextTurn)(int roomId, ClientPointer client))
{
    for (ClientPointer player : players)
    {
        player->setClientState(ClientState::PLAYING_WAITING);
    }
    game = std::make_unique<Pexeso>(id, players, onGameEnd, onNextTurn);
    return *game;
};

bool Room::isInGame()
{
    return game != nullptr;
};

Pexeso &Room::getGame()
{
    return *game;
};

int Room::getId()
{
    return id;
};
