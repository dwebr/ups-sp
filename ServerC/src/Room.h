#pragma once

#include <iostream>
#include <vector>
#include <algorithm>
#include <memory>

#include "Client.h"
#include "Constants.h"
#include "Pexeso.h"

class Room
{
    std::string owner;
    int id;
    std::vector<ClientPointer> players;
    GamePointer game = nullptr;

public:
    Room(int id, std::string owner);
    std::string getOwner();
    int addPlayer(ClientPointer client);
    int removePlayer(ClientPointer client);
    std::vector<ClientPointer> &getPlayers();
    Pexeso &newGame(void (*onGameEnd)(int roomId), void (*onNextTurn)(int roomId, ClientPointer client));
    Pexeso &getGame();
    bool isInGame();
    int getId();
};

typedef std::unique_ptr<Room> RoomPointer;
