
#include "Network.h"

Network::Network(ConfigurationLoader cl, OnMessageReceivedFunction onMessageReceived, OnPlayerConnectionChangeFunction onPlayerConnectionChange)
    : cl(cl), onMessageReceived(onMessageReceived), onPlayerConnectionChange(onPlayerConnectionChange)
{
    buffer.resize(Constants::MAX_MESSAGE_SIZE);
    headerLength = Constants::MESSAGE_PROTOCOL_IDENTIFICATOR.length() + Constants::MESSAGE_LENGTH_DECIMALS + Constants::MESSAGE_TYPE_LENGTH;
};

std::map<int, ClientPointer> &Network::getClients()
{
    return clients;
}

int Network::init()
{
    auto a = AF_INET;
    serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (serverSocket < 0)
    {
        std::cerr << "Cannot create new socket!" << std::endl;
        return 1;
    }

    sockaddr_in serveraddr;
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = serveraddr.sin_port = htons(cl.getIntParameter("SERVER_PORT", Constants::SERVER_PORT));
    serveraddr.sin_addr.s_addr = inet_addr(cl.getStringParameter("SERVER_ADDRESS", Constants::SERVER_ADDRESS).c_str());

    int param = 1;
    int setsockoptResult = setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, (const char *)&param, sizeof(int));
    if (setsockoptResult < 0)
    {
        std::cerr << "Cannot open socket! " << std::endl;
        return 1;
    }

    int bindResult = bind(serverSocket, (sockaddr *)&serveraddr, sizeof(sockaddr_in));
    if (bindResult < 0)
    {
        std::cerr << "Cannot perform binding" << std::endl;
        return 1;
    }

    int listenResult = listen(serverSocket, cl.getIntParameter("MAX_CLIENTS_QUEUE", Constants::MAX_CLIENTS_QUEUE));
    if (listenResult < 0)
    {
        std::cerr << "Cannot create queue of listeners" << std::endl;
        return 1;
    }

    FD_ZERO(&currentSockets);
    FD_SET(serverSocket, &currentSockets);
    FD_SET(0, &currentSockets);
    maxSocket = serverSocket;

    return 0;
}

void Network::clearClient(ClientPointer client, std::string message)
{
    onPlayerConnectionChange(ClientConnectionStatus::DISCONNECTED, client);
    int socketId = client->getSocketId();
    if (message.length() != 0)
    {
        std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, message);
        sendMessageToClient(errorResponse, *client);
    }
    std::cout << "DISCONNECTED " << socketId << " " << client->getNickName() << " " << message << std::endl;
    close(socketId);
    FD_CLR(socketId, &currentSockets);
    clients.erase(socketId);
};

void Network::sendMessageToNonPlayingClients(std::string &body, Client &clientToSkip)
{
    for (auto &clientPair : clients)
    {
        ClientState clientState = clientPair.second->getClientState();
        if (clientState != ClientState::LOBBY && clientState != ClientState::ROOM)
        {
            continue;
        }
        if (clientPair.first == clientToSkip.getSocketId())
        {
            continue;
        }
        sendMessageToClient(body, *clientPair.second);
    }
};

void Network::sendMessageToClient(std::string &body, Client &client, bool log)
{
    if (log)
    {
        std::cout << "SEND TO " << client.getSocketId() << " " << client.getNickName() << ": " << body;
    }
    send(client.getSocketId(), body.c_str(), body.length(), 0);
};

int Network::readHeader(std::string &message, ClientPointer client)
{
    std::string protocolIdentificator;
    ClientMessageType messageType;
    int messageLength;
    int hederDecodedResult = decodeHeader(message, &protocolIdentificator, &messageType, &messageLength);
    if (hederDecodedResult > 0)
    {
        clearClient(client, "Wrong header syntax");
        return 1;
    }

    if (protocolIdentificator != Constants::MESSAGE_PROTOCOL_IDENTIFICATOR)
    {
        clearClient(client, "Wrong protocol identificator");
        return 1;
    }

    if (messageType == ClientMessageType::UNKNOWN)
    {
        clearClient(client, "Unkown message type");
        return 1;
    }

    if (messageLength >= 0)
    {
        client->setExpectedMessageLength(messageLength);
    }
    else
    {
        clearClient(client, "Incorrectly defined message length");
        return 1;
    }
    client->setReceivingMessageType(messageType);
    return 0;
}

void Network::handleNewClient()
{
    int newClientSocket = accept(serverSocket, (sockaddr *)&inaddr, &addrlen);
    if (newClientSocket > 0)
    {
        ClientPointer newClient = std::make_shared<Client>(newClientSocket);
        clients[newClientSocket] = newClient;
        FD_SET(newClientSocket, &currentSockets);

        if (clients.size() > cl.getIntParameter("MAX_CLIENTS", Constants::MAX_CLIENTS))
        {
            clearClient(newClient, "Server is full");
        }
        else
        {
            std::string connectedMessage = encodeResponse(Constants::RESPONSE_SUCCESS, "");
            sendMessageToClient(connectedMessage, *newClient);
            std::cout << "CONNECTED " << newClientSocket << std::endl;
        }

        if (newClientSocket > maxSocket)
        {
            maxSocket = newClientSocket;
        }
    }
    else
    {
        std::cerr << "Error accepting new connection " << std::endl;
    }
}

void Network::handleNewMessage(int clientSocketId)
{
    Client &client = *clients[clientSocketId];

    int expectRecvLen = (client.getBufferIndex() < headerLength) ? headerLength : client.getExpectedMessageLength();
    int receivedDataLength = recv(clientSocketId, client.getBuffer().data() + client.getBufferIndex(), expectRecvLen, 0);
    if (receivedDataLength < 0)
    {
        clearClient(clients[clientSocketId], "Error receiving data");
        return;
    }

    client.getBuffer()[client.getBufferIndex() + receivedDataLength] = '\0';
    client.increaseBufferIndex(receivedDataLength);
    client.setLastActive(time(NULL));

    if (client.getBufferIndex() < headerLength)
    {
        return;
    }

    std::string message = client.getBuffer().data();
    if (client.getReceivingMessageType() == ClientMessageType::UNKNOWN)
    {
        int readHeaderResult = readHeader(message, clients[clientSocketId]);
        if (readHeaderResult > 0)
        {
            return;
        }
    }

    if (client.getBufferIndex() - headerLength == client.getExpectedMessageLength())
    {

        if (clients[clientSocketId]->getReceivingMessageType() == ClientMessageType::PING)
        {
            int pingType;

            if (decodeInt(message.substr(headerLength), &pingType) == 0)
            {
                if (pingType == Constants::PING_LOST_CONNECTION)
                {
                    if (!clients[clientSocketId]->wasStatusMessageSend())
                    {
                        clients[clientSocketId]->setSendStatusMessage(true);
                        onPlayerConnectionChange(ClientConnectionStatus::CONNECTED, clients[clientSocketId]);
                    }
                }
                else
                {
                    clients[clientSocketId]->setSendStatusMessage(false);
                }

                std::string pingResponse = encodeResponse(Constants::RESPONSE_SUCCESS, "");
                sendMessageToClient(pingResponse, *clients[clientSocketId], false);
            }
        }
        else
        {
            clients[clientSocketId]->setSendStatusMessage(false);
            std::cout << "RECEIVED " << clientSocketId << " " << clients[clientSocketId]->getNickName() << ": " << message << std::endl;
            onMessageReceived(message.substr(headerLength), clients[clientSocketId]);
        }
        clients[clientSocketId]->resetBuffer();
    }
}

void Network::receiveMessages()
{
    bool exit = false;
    while (!exit)
    {
        struct timeval timeout = {Constants::TIMEOUT_PENDING_INTERVAL, 0};
        readySockets = currentSockets;
        int selectResult = select(maxSocket + 1, &readySockets, nullptr, nullptr, &timeout);
        if (selectResult < 0)
        {
            std::cerr << "Error on select: " << errno << std::endl;
            return;
        }

        int timeStamp = time(NULL);
        for (int socketId = 0; socketId <= maxSocket; socketId++)
        {
            if (FD_ISSET(socketId, &readySockets) == 0)
            {
                if (clients.find(socketId) != clients.end() && socketId != serverSocket)
                {
                    Client &client = *clients[socketId];
                    int lastComunication = timeStamp - client.getLastActive();
                    if (Constants::TIMEOUT_PENDING_INTERVAL < lastComunication && client.getConnectionStatus() != ClientConnectionStatus::PENDING)
                    {
                        std::cout << "NOT ACTIVE " << socketId << " " << clients[socketId]->getNickName() << std::endl;
                        onPlayerConnectionChange(ClientConnectionStatus::PENDING, clients[socketId]);
                    }
                    else if (Constants::TIMEOUT_DISCONECT_INTERVAL < lastComunication)
                    {
                        clearClient(clients[socketId], "");
                    }
                }
                continue;
            }

            if (socketId == serverSocket)
            {
                handleNewClient();
            }
            else if (socketId == 0)
            {
                exit = true;
                break;
            }
            else
            {
                ioctl(socketId, FIONREAD, &a2read);
                if (a2read <= 0 || a2read > buffer.size())
                {
                    clearClient(clients[socketId], "");
                    continue;
                }

                handleNewMessage(socketId);
            }
        }
    }

    for (auto &clientPair : clients)
    {
        std::cout << "DISCONECTING " << clientPair.first << std::endl;
        close(clientPair.first);
    }
    close(serverSocket);
}
