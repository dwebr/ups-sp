#include "ConfigurationLoader.h"

ConfigurationLoader::ConfigurationLoader(std::string fileName)
{
    std::ifstream file(fileName);

    if (file.is_open())
    {
        std::cout << "Reading configuration from file " << fileName << std::endl;
    }
    else
    {
        std::cerr << "No configuration file loaded, using default configuration" << std::endl;
        return;
    }

    std::string line;
    while (std::getline(file, line))
    {
        std::istringstream is_line(line);
        std::string key;
        if (std::getline(is_line, key, '='))
        {
            std::string value;
            if (std::getline(is_line, value))
            {
                std::cout << key << " : " << value << std::endl;
                std::cout.flush();
                values[key] = value;
            }
        }
    }
    std::cout << "---------" << std::endl;
    file.close();
}

int ConfigurationLoader::getIntParameter(std::string name, int defaultValue)
{
    int value;
    if (values.find(name) == values.end())
    {
        return defaultValue;
    }
    else
    {
        try
        {
            return std::stoi(values[name]);
        }
        catch (...)
        {
            return defaultValue;
        }
    }
}

std::string ConfigurationLoader::getStringParameter(std::string name, std::string defaultValue)
{

    if (values.find(name) == values.end())
    {
        return defaultValue;
    }
    else
    {
        return values[name];
    }
}
