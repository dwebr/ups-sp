#include "Client.h"

Client::Client(int socketId) : socketId(socketId){};

void Client::copyNetworkInfo(Client client)
{
    socketId = client.getSocketId();
    std::copy(std::begin(client.getBuffer()), std::end(client.getBuffer()), std::begin(buffer));
    bufferIndex = client.getBufferIndex();
    expectedMessageLength = client.getExpectedMessageLength();
    lastActive = client.getLastActive();
};

int Client::getSocketId()
{
    return socketId;
};

void Client::setSocketId(int socketId)
{
    this->socketId = socketId;
};

std::string Client::getNickName()
{
    return nickname;
};

void Client::setNickName(std::string nickname)
{
    this->nickname = nickname;
};

int Client::getScore()
{
    return score;
};

void Client::increaseScore()
{
    score++;
};

void Client::resetScore()
{
    score = 0;
};

ClientConnectionStatus Client::getConnectionStatus()
{
    return connectionStatus;
};

void Client::setConnectionStatus(ClientConnectionStatus connectionStatus)
{
    this->connectionStatus = connectionStatus;
};

int Client::getBufferIndex()
{
    return bufferIndex;
};

void Client::increaseBufferIndex(int value)
{
    bufferIndex += value;
};

std::array<char, Constants::MAX_MESSAGE_SIZE> &Client::getBuffer()
{
    return buffer;
};

int Client::getExpectedMessageLength()
{
    return expectedMessageLength;
};

void Client::setExpectedMessageLength(int length)
{
    expectedMessageLength = length;
};

time_t Client::getLastActive()
{
    return lastActive;
};

void Client::setLastActive(time_t time)
{
    lastActive = time;
};

ClientState Client::getClientState()
{
    return state;
};

void Client::setClientState(ClientState state)
{
    this->state = state;
};

void Client::setReceivingMessageType(ClientMessageType receivingMessage)
{
    this->receivingMessage = receivingMessage;
};

ClientMessageType Client::getReceivingMessageType()
{
    return receivingMessage;
};

void Client::setRoomId(int roomId)
{
    this->roomId = roomId;
};

int Client::getRoomId()
{
    return roomId;
};

void Client::setSendStatusMessage(bool sent)
{
    sentStatusMessage = sent;
};

bool Client::wasStatusMessageSend()
{
    return sentStatusMessage;
};

void Client::resetBuffer()
{
    expectedMessageLength = -1;
    bufferIndex = 0;
    receivingMessage = ClientMessageType::UNKNOWN;
};
