#include "Pexeso.h"

Pexeso::Pexeso(int roomId, std::vector<ClientPointer> &players, void (*onGameEnd)(int roomId), void (*onNextTurn)(int roomId, ClientPointer client))
    : roomId(roomId), players(players), onGameEnd(onGameEnd), onNextTurn(onNextTurn)
{
    generateBoard();
    for (ClientPointer player : players)
    {
        player->resetScore();
    }
};

Board &Pexeso::getBoard()
{
    return board;
};

bool Pexeso::revealCard(int id, ClientPointer client)
{
    if (id < 0 || id >= board.size() || lockedTurn || cardRevealedInTurn >= 2)
    {
        return false;
    }
    int card = board[id];
    if (card == -1)
    {
        return false;
    }

    if (cardRevealedInTurn == 0)
    {
        firstRevealedCardId = id;
    }
    else if (board[id] == board[firstRevealedCardId])
    {
        board[id] = -1;
        board[firstRevealedCardId] = -1;
        playerScored = true;
        client->increaseScore();
        scoreAchieved++;
    }

    cardRevealedInTurn++;

    return true;
};

void Pexeso::generateBoard()
{
    std::vector<short> cards;
    cards.resize(Constants::BOARD_SIZE);
    for (int i = 0; i < Constants::BOARD_SIZE; i += 1)
    {
        cards[i] = i / 2;
        cards[i + 1] = i / 2;
    }

    for (int i = 0; i < Constants::BOARD_SIZE; i++)
    {
        int index = rand() % static_cast<int>(cards.size());
        board[i] = cards[index];
        cards.erase(cards.begin() + index);
    }
}

void Pexeso::start()
{
    std::thread timer = std::thread(&Pexeso::roundTimer, this);
    timer.detach();
}

void Pexeso::roundTimer()
{
    while (true)
    {
        lockedTurn = false;
        int disconnectedClients = 0;
        for (ClientPointer client : players)
        {
            if (client->getConnectionStatus() == ClientConnectionStatus::DISCONNECTED)
            {
                disconnectedClients++;
            }
        }

        if (disconnectedClients + Constants::GAME_MIN_NUMBER_OF_PLAYERS > players.size())
        {
            onGameEnd(roomId);
            return;
        }

        players[playerTurnIndex]->setClientState(ClientState::PLAYING_WAITING);

        if (scoreAchieved >= Constants::BOARD_SIZE / 2)
        {
            onGameEnd(roomId);
            return;
        }
        if (playerScored)
        {
        }
        else
        {
            playerTurnIndex = (playerTurnIndex + 1) % players.size();
        }
        playerScored = false;

        if (players[playerTurnIndex]->getConnectionStatus() == ClientConnectionStatus::DISCONNECTED)
        {
            continue;
        }
        cardRevealedInTurn = 0;
        players[playerTurnIndex]->setClientState(ClientState::PLAYING);
        onNextTurn(roomId, players[playerTurnIndex]);
        std::this_thread::sleep_for(std::chrono::seconds(Constants::ROUND_TIMER));
    }
}

std::vector<ClientPointer> &Pexeso::getPlayers()
{
    return players;
}

ClientPointer Pexeso::getPlayerOnTurn()
{
    return players[playerTurnIndex];
};

void Pexeso::lockTurn()
{
    lockedTurn = true;
}