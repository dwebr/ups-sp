#pragma once

#include <iostream>
#include <sys/stat.h>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <algorithm>

/**
 * Loads configuration from file
 */
class ConfigurationLoader
{
    std::map<std::string, std::string> values;

public:
    ConfigurationLoader(std::string fileName);
    int getIntParameter(std::string name, int defaultValue);
    std::string getStringParameter(std::string name, std::string defaultValue);
};
