#include <iostream>
#include <map>
#include <memory>

#include "Network.h"
#include "Pexeso.h"
#include "Client.h"
#include "Room.h"
#include "MessageType.h"
#include "MessageCoder.h"
#include "Constants.h"
#include "ConfigurationLoader.h"

/**
 * Loading configuration from file
 */
ConfigurationLoader cl(Constants::CONFIGURATION_FILE);

/**
 * Asynchronous methods definition
 */
void onMessageReceived(std::string messageBody, ClientPointer client);
void onPlayerConnectionChange(ClientConnectionStatus status, ClientPointer client);
void onGameEnd(int roomId);
void onPlayersTurn(int roomId, ClientPointer client);

Network network(cl, onMessageReceived, onPlayerConnectionChange);
std::map<int, RoomPointer> rooms;

/**
 * Enetry point of application
 */
int main(int argc, char **argv)
{
    if (network.init() == 0)
    {
        std::cout << "Server successfully started" << std::endl;
        std::cout << "Press enter to shutdown..." << std::endl;
        network.receiveMessages();
    }
    else
    {
        std::cerr << "Error initializing server" << std::endl;
    }
    std::cout << "Server shutdown" << std::endl;
    return 0;
}

/**
 * Sends message with full information about his state in game/lobby
 */
void sendActualStateToPlayer(ClientPointer client)
{
    ClientState state = client->getClientState();
    if (state == ClientState::PLAYING || state == ClientState::PLAYING_WAITING)
    {
        std::string gameStatusMessage = encodeGameStatus(rooms[client->getRoomId()]->getGame());
        network.sendMessageToClient(gameStatusMessage, *client);
    }
    else if (state == ClientState::LOBBY || state == ClientState::ROOM)
    {
        std::string roomListMessage = encodeRoomList(rooms);
        network.sendMessageToClient(roomListMessage, *client);
    }
}

/**
 * Creates new room
 * returns room id
 */
int createNewRoom(std::string owner)
{
    int lowestId = 1;
    for (auto &roomPair : rooms)
    {
        int id = roomPair.first;
        if (id == lowestId)
            lowestId++;
        else
            break;
    }
    rooms[lowestId] = std::make_unique<Room>(lowestId, owner);
    return lowestId;
}

// ================================================================================
// Section - handeling client requests
// ================================================================================

void onLogin(ClientPointer client, std::string nick)
{

    if (client->getClientState() != ClientState::LOGIN)
    {
        std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "Client already logged in");
        network.sendMessageToClient(errorResponse, *client);
        return;
    }

    std::map<int, ClientPointer> &clients = network.getClients();

    for (auto &clientPair : network.getClients())
    {
        if (clientPair.second->getNickName() == nick)
        {
            std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "This nick is already used");
            network.sendMessageToClient(errorResponse, *client);
            return;
        }
    }

    if (nick.length() < Constants::MINIMAL_NICK_LENGTH || nick.length() > Constants::MAXIMAL_NICK_LENGTH)
    {
        std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "Invalid nick length");
        network.sendMessageToClient(errorResponse, *client);
        return;
    }

    std::string loggedInResponse = encodeResponse(Constants::RESPONSE_SUCCESS, "");
    network.sendMessageToClient(loggedInResponse, *client);

    // checks if player is in game
    for (auto &roomPair : rooms)
    {
        if (roomPair.second->isInGame())
        {
            for (ClientPointer player : roomPair.second->getPlayers())
            {
                if (player->getNickName() == nick && player->getConnectionStatus() == ClientConnectionStatus::DISCONNECTED)
                {
                    player->copyNetworkInfo(*client);
                    clients[client->getSocketId()] = player;
                    onPlayerConnectionChange(ClientConnectionStatus::CONNECTED, player);
                    return;
                }
            }
        }
    }

    std::string roomListMessage = encodeRoomList(rooms);
    network.sendMessageToClient(roomListMessage, *client);

    client->setNickName(nick);
    client->setClientState(ClientState::LOBBY);
}

void onRoomCreate(ClientPointer client)
{
    if (client->getClientState() != ClientState::LOBBY)
    {
        std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "You are not in lobby");
        network.sendMessageToClient(errorResponse, *client);
        sendActualStateToPlayer(client);
        return;
    }

    if (rooms.size() >= cl.getIntParameter("MAX_ROOM_COUNT", Constants::MAX_ROOM_COUNT))
    {
        std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "No capacity for new room");
        network.sendMessageToClient(errorResponse, *client);
        return;
    }

    int roomId = createNewRoom(client->getNickName());
    Room &room = *rooms[roomId];

    std::string encodedRoomId = encodeNumber(room.getId(), Constants::MESSAGE_ROOM_ID_DECIMALS);
    std::string roomCreatedResponse = encodeResponse(Constants::RESPONSE_SUCCESS, encodedRoomId);
    network.sendMessageToClient(roomCreatedResponse, *client);

    std::string roomListMessage = encodeRoomList(rooms);
    network.sendMessageToNonPlayingClients(roomListMessage, *client);
}

void onRoomDelete(ClientPointer client, Room &room)
{
    if (client->getClientState() != ClientState::ROOM)
    {
        std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "You are not in room");
        network.sendMessageToClient(errorResponse, *client);
        sendActualStateToPlayer(client);
        return;
    }

    if (client->getNickName() != room.getOwner())
    {
        std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "You are not the owner");
        network.sendMessageToClient(errorResponse, *client);
        sendActualStateToPlayer(client);
        return;
    }

    if (room.isInGame())
    {
        std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "Room is in game");
        network.sendMessageToClient(errorResponse, *client);
        sendActualStateToPlayer(client);
        return;
    }

    client->setClientState(ClientState::LOBBY);
    for (ClientPointer clientInRoom : room.getPlayers())
    {
        clientInRoom->setClientState(ClientState::LOBBY);
        clientInRoom->setRoomId(-1);
    }
    rooms.erase(room.getId());

    std::string clientDeletedResponse = encodeResponse(Constants::RESPONSE_SUCCESS, "");
    network.sendMessageToClient(clientDeletedResponse, *client);

    std::string roomListMessage = encodeRoomList(rooms);
    network.sendMessageToNonPlayingClients(roomListMessage, *client);
}

void onRoomEnter(ClientPointer client, Room &room)
{
    if (client->getClientState() != ClientState::LOBBY)
    {
        std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "You are not in lobby");
        network.sendMessageToClient(errorResponse, *client);
        sendActualStateToPlayer(client);
        return;
    }

    if (room.isInGame())
    {
        std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "Room is in game");
        network.sendMessageToClient(errorResponse, *client);
        sendActualStateToPlayer(client);
        return;
    }

    int playerEnteredResult = room.addPlayer(client);
    if (playerEnteredResult > 0)
    {
        std::string roomErrorResponse = encodeResponse(Constants::RESPONSE_FAIL, "Room is full");
        network.sendMessageToClient(roomErrorResponse, *client);
        sendActualStateToPlayer(client);
        return;
    }
    client->setRoomId(room.getId());
    client->setClientState(ClientState::ROOM);

    std::string clientEnteredResponse = encodeResponse(Constants::RESPONSE_SUCCESS, "");
    network.sendMessageToClient(clientEnteredResponse, *client);

    std::string roomListMessage = encodeRoomList(rooms);
    network.sendMessageToNonPlayingClients(roomListMessage, *client);
}

void onRoomLeave(ClientPointer client)
{
    if (client->getClientState() != ClientState::ROOM)
    {
        std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "You are not in room");
        network.sendMessageToClient(errorResponse, *client);
        sendActualStateToPlayer(client);
        return;
    }
    Room &room = *rooms[client->getRoomId()];
    client->setRoomId(-1);
    client->setClientState(ClientState::LOBBY);
    room.removePlayer(client);

    std::string clientLeftResponse = encodeResponse(Constants::RESPONSE_SUCCESS, "");
    network.sendMessageToClient(clientLeftResponse, *client);

    std::string roomListMessage = encodeRoomList(rooms);
    network.sendMessageToNonPlayingClients(roomListMessage, *client);
}

void onRoomStart(ClientPointer client, Room &room)
{
    if (room.isInGame())
    {
        std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "Room is already in game");
        network.sendMessageToClient(errorResponse, *client);
        sendActualStateToPlayer(client);
        return;
    }

    if (client->getRoomId() != room.getId())
    {
        std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "You are not in the room");
        network.sendMessageToClient(errorResponse, *client);
        sendActualStateToPlayer(client);
        return;
    }

    if (client->getNickName() != room.getOwner())
    {
        std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "You are not the owner");
        network.sendMessageToClient(errorResponse, *client);
        sendActualStateToPlayer(client);
        return;
    }

    if (room.getPlayers().size() < Constants::GAME_MIN_NUMBER_OF_PLAYERS)
    {
        std::string roomErrorResponse = encodeResponse(Constants::RESPONSE_FAIL, "Not enough players");
        network.sendMessageToClient(roomErrorResponse, *client);
        sendActualStateToPlayer(client);
        return;
    }

    std::string roomStartedResponse = encodeResponse(Constants::RESPONSE_SUCCESS, "");
    network.sendMessageToClient(roomStartedResponse, *client);

    Pexeso &game = room.newGame(onGameEnd, onPlayersTurn);

    std::string roomStartMessage = encodeStartGame(game.getBoard());
    for (ClientPointer player : room.getPlayers())
    {
        player->setClientState(ClientState::PLAYING_WAITING);
        network.sendMessageToClient(roomStartMessage, *player);
    }

    std::string roomListMessage = encodeRoomList(rooms);
    network.sendMessageToNonPlayingClients(roomListMessage, *client);

    game.start();
}

void onRevealCard(ClientPointer client, int cardId)
{
    Room &room = *rooms[client->getRoomId()];

    if (client->getClientState() != ClientState::PLAYING)
    {
        std::string roomErrorResponse = encodeResponse(Constants::RESPONSE_FAIL, "It is not your game turn");
        network.sendMessageToClient(roomErrorResponse, *client);
        return;
    }

    bool cardRevealed = room.getGame().revealCard(cardId, client);
    if (!cardRevealed)
    {
        std::string cardRevealedResponse = encodeResponse(Constants::RESPONSE_FAIL, "Card reveal failed");
        network.sendMessageToClient(cardRevealedResponse, *client);
        return;
    }

    std::string cardRevealedResponse = encodeResponse(Constants::RESPONSE_SUCCESS, "");
    network.sendMessageToClient(cardRevealedResponse, *client);

    std::string cardRevealedMessage = encodeRevealCard(cardId);
    for (ClientPointer player : room.getPlayers())
    {
        if (player->getSocketId() != client->getSocketId() && player->getConnectionStatus() == ClientConnectionStatus::CONNECTED)
        {
            network.sendMessageToClient(cardRevealedMessage, *player);
        }
    }
}

/**
 * Called when new message received
 * Decodes message and call function to handle 
 */
void onMessageReceived(std::string messageBody, ClientPointer client)
{
    ClientMessageType messageType = client->getReceivingMessageType();
    switch (messageType)
    {

    case ClientMessageType::LOGIN:
    {
        std::string nick;
        if (decodeString(messageBody, &nick, Constants::MESSAGE_NICK_DECIMALS) == 0)
        {
            onLogin(client, nick);
        }
        else
        {
            std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "Error decoding nick");
            network.sendMessageToClient(errorResponse, *client);
        }
        break;
    }

    case ClientMessageType::ROOM_CREATE:
    {
        onRoomCreate(client);
        break;
    }

    case ClientMessageType::ROOM_DELETE:
    {
        int roomId;
        if (decodeInt(messageBody, &roomId) == 0 && rooms.find(roomId) != rooms.end())
        {
            onRoomDelete(client, *rooms[roomId]);
        }
        else
        {
            std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "Error - room id invalid");
            network.sendMessageToClient(errorResponse, *client);
        }
        break;
    }

    case ClientMessageType::ROOM_ENTER:
    {
        int roomId;
        if (decodeInt(messageBody, &roomId) == 0 && rooms.find(roomId) != rooms.end())
        {
            onRoomEnter(client, *rooms[roomId]);
        }
        else
        {
            std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "Error - room id invalid");
            network.sendMessageToClient(errorResponse, *client);
        }
        break;
    }

    case ClientMessageType::ROOM_LEAVE:
    {
        onRoomLeave(client);
        break;
    }

    case ClientMessageType::ROOM_START:
    {
        int roomId;
        if (decodeInt(messageBody, &roomId) == 0 && rooms.find(roomId) != rooms.end())
        {
            onRoomStart(client, *rooms[roomId]);
        }
        else
        {
            std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "Error - room id invalid");
            network.sendMessageToClient(errorResponse, *client);
        }
        break;
    }

    case ClientMessageType::REVEAL_CARD:
    {
        int cardId;
        if (decodeInt(messageBody, &cardId) == 0)
        {
            onRevealCard(client, cardId);
        }
        else
        {
            std::string errorResponse = encodeResponse(Constants::RESPONSE_FAIL, "Error decoding room id");
            network.sendMessageToClient(errorResponse, *client);
        }
        break;
    }
    }
}

void onPlayerConnectionChange(ClientConnectionStatus newStatus, ClientPointer client)
{

    ClientConnectionStatus oldStatus = client->getConnectionStatus();
    client->setConnectionStatus(newStatus);
    if (newStatus == ClientConnectionStatus::CONNECTED)
    {
        sendActualStateToPlayer(client);
    }

    // player disconnected
    if (newStatus == ClientConnectionStatus::DISCONNECTED)
    {
        bool roomDeleted = false;

        // leaving if in room
        if (client->getClientState() == ClientState::ROOM)
        {
            Room &room = *rooms[client->getRoomId()];
            room.removePlayer(client);

            std::string roomListMessage = encodeRoomList(rooms);
            network.sendMessageToNonPlayingClients(roomListMessage, *client);
        }

        // deletes all players rooms that are not in game
        for (auto roomIterator = rooms.begin(), last = rooms.end(); roomIterator != last;)
        {
            if (!roomIterator->second->isInGame() && roomIterator->second->getOwner() == client->getNickName())
            {
                roomDeleted = true;
                roomIterator = rooms.erase(roomIterator);
            }
            else
            {
                ++roomIterator;
            }
        }
        if (roomDeleted)
        {
            std::string roomListMessage = encodeRoomList(rooms);
            network.sendMessageToNonPlayingClients(roomListMessage, *client);
        }
    }

    // if player left while playing game
    if (client->getClientState() == ClientState::PLAYING || client->getClientState() == ClientState::PLAYING_WAITING)
    {
        // locks turn if player connects while is his turn
        Room &room = *rooms[client->getRoomId()];
        if (client->getClientState() == ClientState::PLAYING)
        {
            room.getGame().lockTurn();
        }

        // sends message to other players
        std::string clientConnectionMessage = encodePlayersConnection(client->getNickName(), newStatus);
        for (ClientPointer player : room.getPlayers())
        {
            if (player->getSocketId() != client->getSocketId() && player->getConnectionStatus() == ClientConnectionStatus::CONNECTED)
            {
                network.sendMessageToClient(clientConnectionMessage, *player);
            }
        }
    }
}

// ================================================================================
// Section - methods called when game changes state
// ================================================================================

void onPlayersTurn(int roomId, ClientPointer client)
{
    std::string playerTurnMessage = encodePlayersTurn(client->getNickName());
    for (ClientPointer player : rooms[roomId]->getPlayers())
    {
        if (player->getConnectionStatus() == ClientConnectionStatus::CONNECTED)
        {
            network.sendMessageToClient(playerTurnMessage, *player);
        }
    }
}

void onGameEnd(int roomId)
{
    std::string gameEndMessage = encodeGameEnd();

    Room &room = *rooms[roomId];
    std::vector<ClientPointer> players = room.getPlayers();
    rooms.erase(roomId);
    std::string roomListMessage = encodeRoomList(rooms);
    for (ClientPointer player : players)
    {
        if (player->getConnectionStatus() == ClientConnectionStatus::CONNECTED)
        {
            network.sendMessageToClient(gameEndMessage, *player);
            player->setClientState(ClientState::LOBBY);
            network.sendMessageToClient(roomListMessage, *player);
        }
    }
}
