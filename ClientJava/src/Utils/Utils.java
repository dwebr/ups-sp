package Utils;

/**
 * Class containing utility static methods
 */
public class Utils {
    /**
     * Add padding to number
     * 
     * @param number
     * @param length required length
     * @return
     */
    public static String getFormattedNumber(int number, int length) {
        return String.format("%0" + length + "d", number);
    }

    /**
     * returns ordinal
     * 
     * @param position
     * @return
     */
    public static String getOrdinal(int position) {
        String[] suffixes = new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };
        switch (position % 100) {
            case 11:
            case 12:
            case 13:
                return position + "th";
            default:
                return position + suffixes[position % 10];

        }
    }
}
