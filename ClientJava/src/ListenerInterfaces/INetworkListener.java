package ListenerInterfaces;

import java.util.ArrayList;

import Objects.ConnectionStatusType;
import Objects.Player;
import Objects.Request;
import Objects.Room;

public interface INetworkListener {

    /**
     * Called when received expected response from server
     * 
     * @param responseTo      last sent request
     * @param responseCode    response code of response
     * @param responseMessage response message
     */
    public void onServerResponse(Request responseTo, int responseCode, String responseMessage);

    /**
     * Called when received list of all waiting rooms from server
     * 
     * @param rooms
     */
    public void onServerMessageRoomList(ArrayList<Room> rooms);

    /**
     * Called when received game board from server
     * 
     * @param board
     */
    public void onServerMessageStartGame(int[] board);

    /**
     * Called whn received players turn from server
     * 
     * @param nick
     */
    public void onServerMessagePlayersTurn(String nick);

    /**
     * Called when received player revealed card from server
     * 
     * @param cardId
     */
    public void onServerMessageRevealCard(int cardId);

    /**
     * Called when player connection change received from server
     * 
     * @param connectionStatus
     * @param nick
     */
    public void onServerMessagePlayerConnectionStatus(ConnectionStatusType connectionStatus, String nick);

    /**
     * Called when received from server game ends
     */
    public void onServerMessageGameEnd();

    /**
     * Called when received status about playing game from server
     * 
     * @param board
     * @param playerOnTurn
     * @param players
     */
    public void onServerGameStatus(int[] board, String playerOnTurn, ArrayList<Player> players);

    /**
     * Called when received invalid message from server
     * 
     * @param message error message
     */
    public void onServerMessageInvalid(String message);

    /**
     * Called when connection with server changes
     * 
     * @param connectionStatus
     */
    public void onServerConnectionChanged(ConnectionStatusType connectionStatus);

}
