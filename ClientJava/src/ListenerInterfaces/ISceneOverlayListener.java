package ListenerInterfaces;

public interface ISceneOverlayListener {
    /**
     * Called when connect button pressed
     */
    public void userConnect();

    /**
     * Called when logout button pressed
     */
    public void userLogOut();
}
