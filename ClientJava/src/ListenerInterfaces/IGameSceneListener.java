package ListenerInterfaces;

public interface IGameSceneListener {
    /**
     * called when user clicked on card
     * 
     * @param cardID
     */
    public void onUserRevealCard(int cardID);
}