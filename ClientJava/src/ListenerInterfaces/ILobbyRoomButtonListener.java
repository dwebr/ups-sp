package ListenerInterfaces;

public interface ILobbyRoomButtonListener {
    /**
     * called when button pressed
     * 
     * @param id
     */
    public void onButtonPressed(int id);
}
