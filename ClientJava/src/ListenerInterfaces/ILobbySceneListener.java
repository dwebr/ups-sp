package ListenerInterfaces;

public interface ILobbySceneListener {
    /**
     * called when user pressed button create new room
     */
    public void onUserCreateNewRoom();

    /**
     * called when user pressed button with existing room
     * 
     * @param id
     */
    public void onUserEnterRoom(int id);
}
