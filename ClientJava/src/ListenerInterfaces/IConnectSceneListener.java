package ListenerInterfaces;

public interface IConnectSceneListener {
    /**
     * Called when user tries to connect with valid ipAddress and port
     * 
     * @param ipAddress
     * @param port
     */
    public void onUserConnect(String ipAddress, int port);

    /**
     * Called when user tries to login with valid nick
     * 
     * @param nick
     */
    public void onUserLogin(String nick);
}
