package ListenerInterfaces;

public interface ICardPanelListener {
    /**
     * Called when clicked on card
     * 
     * @param cardId
     * @param cardType
     */
    public void onCardPressed(int cardId, int cardType);
}
