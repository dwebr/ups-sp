package ListenerInterfaces;

public interface IEndGameSceneListener {
    /**
     * Called when user clicked back to lobby button
     */
    public void onUserBackToLobby();
}
