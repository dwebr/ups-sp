package ListenerInterfaces;

import java.util.ArrayList;

import Objects.ConnectionStatusType;
import Objects.Player;
import Objects.Room;

public interface INetworkMessageReceiverListener {

    /**
     * called when response received from server
     * 
     * @param responseCode
     * @param message
     */
    public void onResponseReceived(int responseCode, String message);

    /**
     * Called when received list of all waiting rooms from server
     * 
     * @param rooms
     */
    public void onRoomListReceived(ArrayList<Room> rooms);

    /**
     * Called when received game start from server
     * 
     * @param board
     */
    public void onStartGameReceived(int[] board);

    /**
     * Called when players turn received from server
     * 
     * @param nick
     */
    public void onPlayersTurnReceived(String nick);

    /**
     * Called when reveal card received from server
     * 
     * @param cardId
     */
    public void onRevealCardReceived(int cardId);

    /**
     * Called when player connection status changed received from server
     * 
     * @param connectionStatus
     * @param nick
     */
    public void onPlayerConnectionStatusReceived(ConnectionStatusType connectionStatus, String nick);

    /**
     * Called when on game ends received from server
     */
    public void onGameEndReceived();

    /**
     * Called when invalid message received from server
     * 
     * @param message
     */
    public void onInvalidMessageReceived(String message);

    /**
     * Called when game status received from server
     * 
     * @param board
     * @param playerOnTurn
     * @param players
     */
    public void onGameStatusReceived(int[] board, String playerOnTurn, ArrayList<Player> players);

    /**
     * Called when connection is closed
     */
    public void onConnectionClosed();

}
