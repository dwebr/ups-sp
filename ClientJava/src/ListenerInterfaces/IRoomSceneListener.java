package ListenerInterfaces;

public interface IRoomSceneListener {
    /**
     * Called when leave room button pressed
     * 
     * @param id
     */
    public void userLeaveRoom(int id);

    /**
     * Called when delete room button pressed
     * 
     * @param id
     */
    public void userDeleteRoom(int id);

    /**
     * Called when start room button pressed
     * 
     * @param id
     */
    public void userStartRoom(int id);
}
