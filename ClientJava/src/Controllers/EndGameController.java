package Controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import ListenerInterfaces.IEndGameSceneListener;
import Objects.Player;
import Utils.Utils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class EndGameController {

    private IEndGameSceneListener listener;
    private String userNick;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label playersPlaceLabel;

    @FXML
    private VBox playersVBox;

    @FXML
    private Button backToLobbyButton;

    @FXML
    void onBactToLobbyButtonPressed(ActionEvent event) {
        listener.onUserBackToLobby();
    }

    public EndGameController(IEndGameSceneListener listener, String userNick) {
        this.listener = listener;
        this.userNick = userNick;
    }

    public void setPlayers(ArrayList<Player> players) {
        playersVBox.getChildren().clear();
        int place = 0;
        int score = Integer.MAX_VALUE;
        int skipped = 0;
        for (Player player : players) {
            skipped++;
            if (player.getScore() < score) {
                place = place + skipped;
                skipped = 0;
                score = player.getScore();
            }
            Label label = new Label(place + ". " + player.getNick() + ": " + score + "p.");
            if (place == 1) {
                label.getStyleClass().add("winnersLabelFirst");
            } else {
                label.getStyleClass().add("winnersLabelOthers");
            }

            playersVBox.getChildren().add(label);
            if (player.getNick().equals(userNick)) {
                playersPlaceLabel.setText(Utils.getOrdinal(place) + " place!");
            }

        }

    }

    @FXML
    void initialize() {

    }

}
