package Controllers;

import java.net.URL;
import java.util.ResourceBundle;

import Objects.ConnectionStatusType;
import Objects.Player;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class PlayerPanelController {

    private Player player;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private VBox playersVBox;

    @FXML
    private Label playersNickLabel;

    @FXML
    private Label scoreLabel;

    @FXML
    private Label connectionLabel;

    @FXML
    void initialize() {
        playersNickLabel.setText(player.getNick());
        scoreLabel.setText("Score: " + player.getScore());
        setConnectionStatus(player.getConnectionStatus());
    }

    public PlayerPanelController(Player player) {
        this.player = player;
    }

    public void setScore(int score) {
        player.setScore(score);
        scoreLabel.setText("Score: " + score);
    }

    public void setConnectionStatus(ConnectionStatusType connectionStatus) {
        player.setConnectionStatus(connectionStatus);
        switch (connectionStatus) {
            case CONNECTED: {
                connectionLabel.setText("Connected");
                connectionLabel.getStyleClass().removeAll("warning", "danger");
                connectionLabel.getStyleClass().add("success");
                return;
            }
            case DISCONNECTED: {
                connectionLabel.setText("Disconnected");
                connectionLabel.getStyleClass().removeAll("warning", "success");
                connectionLabel.getStyleClass().add("danger");
                return;
            }

            case PENDING: {
                connectionLabel.setText("Pending...");
                connectionLabel.getStyleClass().removeAll("success", "danger");
                connectionLabel.getStyleClass().add("warning");
                return;
            }

        }
    }

    public void increaseScore() {
        setScore(player.getScore() + 1);
    }

    public void setHighlited(boolean highlited) {
        if (highlited) {
            playersVBox.setStyle("-fx-background-color: #f4f5f3;");
        } else {
            playersVBox.setStyle("");
        }
    }

    public Player getPlayer() {
        return player;
    }

}
