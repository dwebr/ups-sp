package Controllers;

import java.net.URL;
import java.util.ResourceBundle;

import Constants.Constants;
import ListenerInterfaces.ICardPanelListener;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;

public class CardController {
    private ICardPanelListener listener;
    private int cardId;
    private int cardType;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Pane cardPanel;

    @FXML
    void onPress(MouseEvent event) {
        listener.onCardPressed(cardId, cardType);
    }

    @FXML
    void initialize() {
    }

    public CardController(ICardPanelListener listener, int cardId, int cardType) {
        this.listener = listener;
        this.cardId = cardId;
        this.cardType = cardType;
    }

    public void reveal() {
        cardPanel.setDisable(true);
        String url = getClass().getResource(Constants.CARD_IMAGE_LOCATION + cardType + ".png").toString();
        BackgroundImage myBI = new BackgroundImage(new Image(url, 100, 100, false, true), BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        cardPanel.setBackground(new Background(myBI));
        cardPanel.getStyleClass().removeAll(cardPanel.getStyleClass());
    }

    public void hide() {
        cardPanel.setDisable(false);
        cardPanel.getStyleClass().addAll("panewarning");
    }

    public void remove() {
        cardPanel.setVisible(false);
    }

    public int getCardId() {
        return cardId;
    }

    public int getCardType() {
        return cardType;
    }

}
