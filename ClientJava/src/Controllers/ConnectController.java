package Controllers;

import java.net.URL;
import java.text.Normalizer;
import java.util.ResourceBundle;

import ListenerInterfaces.IConnectSceneListener;
import Constants.Constants;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import org.apache.commons.lang3.StringUtils;

public class ConnectController {

    private IConnectSceneListener listener;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField ipAddressInput;

    @FXML
    private TextField portInput;

    @FXML
    private Button connectButton;

    @FXML
    private Label connectionLabel;

    @FXML
    private TextField nickInput;

    @FXML
    private Button loginButton;

    @FXML
    private Label errorLabel;

    @FXML
    private Label versionLabel;

    public ConnectController(IConnectSceneListener listener) {
        this.listener = listener;

    }

    @FXML
    void initialize() {
        setInputOnlyNumerical(portInput);
        versionLabel.setText("Version: " + Constants.VERSION);
    }

    @FXML
    private void ConnectButtonPressed(ActionEvent event) {

        String ipAddress = ipAddressInput.getText();

        if (StringUtils.isBlank(ipAddress)) {
            errorLabel.setText("IP address is required!");
            cleanConnectionLabel();
            return;
        }
        if (!ipAddressValid(ipAddress)) {
            errorLabel.setText("IP address is not valid!");
            cleanConnectionLabel();
            return;
        }

        if (StringUtils.isBlank(portInput.getText())) {
            errorLabel.setText("Port is required!");
            cleanConnectionLabel();
            return;
        }
        int port = Integer.parseInt(portInput.getText());
        if (!isPortValid(port)) {
            errorLabel.setText("Port is not valid!");
            cleanConnectionLabel();
            return;
        }

        errorLabel.setText("");
        connectionLabel.setText("Connecting...");
        connectionLabel.getStyleClass().remove("danger");
        connectionLabel.getStyleClass().add("warning");
        connectButton.setDisable(true);
        listener.onUserConnect(ipAddress, port);
    }

    @FXML
    private void LoginButtonPressed(ActionEvent event) {
        String nick = nickInput.getText();
        if (StringUtils.isBlank(nick)) {
            errorLabel.setText("Nick is required!");
            return;
        }
        if (!isNickValid(nick)) {
            errorLabel.setText("Nick must be loner than " + Constants.MINIMAL_NICK_LENGTH + " and shorter than "
                    + Constants.MAXIMAL_NICK_LENGTH + "!");
            return;
        }
        if (!Normalizer.isNormalized(nick, Normalizer.Form.NFD)) {
            errorLabel.setText("Nick contains unsupported characters");
            return;
        }
        errorLabel.setText("");
        listener.onUserLogin(nick);
    }

    public void setConnected(boolean connected) {
        connectionLabel.getStyleClass().remove("warning");
        connectionLabel.getStyleClass().remove("danger");
        connectionLabel.getStyleClass().remove("success");
        if (connected) {
            ipAddressInput.setDisable(true);
            portInput.setDisable(true);
            loginButton.setDisable(false);
            connectionLabel.setText("Connected");
            connectionLabel.getStyleClass().add("success");
        } else {
            connectButton.setDisable(false);
            connectionLabel.setText("No connection");
            ipAddressInput.setDisable(false);
            portInput.setDisable(false);
            loginButton.setDisable(true);
            connectionLabel.getStyleClass().add("danger");
        }
    }

    public void setLoginFailed() {
        connectionLabel.getStyleClass().remove("warning");
        errorLabel.setText("Login failed");
    }

    public void cleanConnectionLabel() {
        connectionLabel.setText("not connected");
        connectionLabel.getStyleClass().remove("danger");
    }

    private boolean ipAddressValid(String ipAddress) {
        String PATTERN = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";
        return ipAddress.matches(PATTERN);

    }

    private boolean isPortValid(int port) {
        boolean valid = port <= 65535 && port >= 0;
        return valid;
    }

    private boolean isNickValid(String nick) {
        boolean valid = nick.length() <= Constants.MAXIMAL_NICK_LENGTH
                && nick.length() >= Constants.MINIMAL_NICK_LENGTH;
        return valid;
    }

    private void setInputOnlyNumerical(TextField input) {
        input.textProperty().addListener(new javafx.beans.value.ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    input.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
    }

}
