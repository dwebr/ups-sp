package Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import Constants.Constants;
import ListenerInterfaces.ILobbyRoomButtonListener;
import Objects.Room;
import javafx.scene.layout.HBox;

public class LobbyRoomButtonController {

    private Room room;
    private ILobbyRoomButtonListener listener;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label roomNameLabel;

    @FXML
    private Label roomOwnerLabel;

    @FXML
    private Label playersLabel;

    @FXML
    private HBox rootHBox;

    @FXML
    void EnterRoomPressed(MouseEvent event) {
        listener.onButtonPressed(room.getId());
    }

    @FXML
    void initialize() {
        roomNameLabel.setText("Room " + room.getId());
        roomOwnerLabel.setText("Owner: " + room.getOwner());
        playersLabel.setText(room.getPlayersCount() + "/" + Constants.MAX_NUMBER_OF_PLAYERS);
        if (room.getPlayersCount() >= Constants.MAX_NUMBER_OF_PLAYERS)
            rootHBox.setDisable(true);
    }

    public LobbyRoomButtonController(Room room, ILobbyRoomButtonListener listener) {
        this.room = room;
        this.listener = listener;
    }

    public int getRoomId() {
        return room.getId();
    }

}
