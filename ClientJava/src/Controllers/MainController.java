package Controllers;

import java.io.IOException;
import java.util.ArrayList;

import ListenerInterfaces.IConnectSceneListener;
import ListenerInterfaces.IEndGameSceneListener;
import ListenerInterfaces.IGameSceneListener;
import ListenerInterfaces.ILobbySceneListener;
import ListenerInterfaces.INetworkListener;
import ListenerInterfaces.IRoomSceneListener;
import ListenerInterfaces.ISceneOverlayListener;
import Network.Network;
import Objects.ConnectionStatusType;
import Objects.Player;
import Objects.Request;
import Objects.RequestType;
import Objects.Room;
import Objects.SceneType;
import Constants.Constants;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Enables communication between network and other controllers Manages switching
 * scenes
 */
public class MainController implements IConnectSceneListener, ILobbySceneListener, INetworkListener, IRoomSceneListener,
    IGameSceneListener, IEndGameSceneListener, ISceneOverlayListener {

  private final Network network;
  private final Stage window;

  private SceneType currentScene;
  private String userNick = "";
  private String ipAddress;
  private int port;

  private SceneOverlayController sceneOverlayController;
  private ConnectController connectController;
  private Parent connectScene;
  private LobbyController lobbyController;
  private Parent lobbyScene;
  private RoomController roomController;
  private Parent roomScene;
  private GameController gameController;
  private Parent gameScene;
  private EndGameController endGameController;
  private Parent endGameScene;

  public MainController(Stage window, Network network) {
    this.network = network;
    this.window = window;
    network.setListener(this);
    window.setOnCloseRequest(e -> {
      network.closeConnection();
      System.exit(0);
    });
    loadSceneOverLay();
    showScene(SceneType.CONNECT);
    window.setTitle(Constants.STAGE_TITLE);
    window.setResizable(false);
    window.show();
  }

  // ================================================================================
  // Section - scene management
  // ================================================================================

  /**
   * show scene at stage
   * 
   * @param sceneType which scene to display
   */
  private void showScene(SceneType sceneType) {
    sceneOverlayController.setMessage("");
    if (currentScene == sceneType)
      return;
    switch (sceneType) {
      case CONNECT: {
        currentScene = SceneType.CONNECT;
        if (connectScene != null) {
          sceneOverlayController.setScene(connectScene);
          return;
        }
        connectController = new ConnectController(this);
        connectScene = loadScene(Constants.CONNECT_VIEW_LOCATION, connectController);
        return;
      }

      case LOBBY: {
        currentScene = SceneType.LOBBY;
        if (lobbyScene != null) {
          sceneOverlayController.setScene(lobbyScene);
          return;
        }
        lobbyController = new LobbyController(this);
        lobbyScene = loadScene(Constants.LOBBY_VIEW_LOCATION, lobbyController);
        return;
      }

      case ROOM:
        currentScene = SceneType.ROOM;
        if (roomScene != null) {
          sceneOverlayController.setScene(roomScene);
          return;
        }
        roomController = new RoomController(this, userNick);
        roomScene = loadScene(Constants.ROOM_VIEW_LOCATION, roomController);
        return;

      case GAME:
        currentScene = SceneType.GAME;
        if (gameScene != null) {
          sceneOverlayController.setScene(gameScene);
          return;
        }
        gameController = new GameController(this, userNick);
        gameScene = loadScene(Constants.GAME_VIEW_LOCATION, gameController);
        return;

      case END_GAME:
        currentScene = SceneType.END_GAME;
        if (endGameScene != null) {
          sceneOverlayController.setScene(endGameScene);
          return;
        }
        endGameController = new EndGameController(this, userNick);
        endGameScene = loadScene(Constants.END_GAME_VIEW_LOCATION, endGameController);
        return;
      default:
        break;
    }

  }

  /**
   * Loads scene from resource folder and pair it with controller
   * 
   * @param resource   resource name
   * @param controller controller to pair with scene
   * @return scene
   */
  private Parent loadScene(String resource, Object controller) {
    Parent scene = null;
    FXMLLoader loader = new FXMLLoader(getClass().getResource(resource));

    loader.setController(controller);
    try {
      scene = loader.load();
      sceneOverlayController.setScene(scene);
    } catch (IOException e) {
      e.printStackTrace();
      sceneOverlayController.setMessage("Next scene could not be loaded");
    }
    return scene;
  }

  /**
   * Loads screen overlay
   */
  public void loadSceneOverLay() {
    sceneOverlayController = new SceneOverlayController(this);
    FXMLLoader loader = new FXMLLoader(getClass().getResource(Constants.SCENE_OVERLAY_VIEW_LOCATION));
    loader.setController(sceneOverlayController);
    try {
      Scene sceneOverlay = new Scene(loader.load());
      window.setScene(sceneOverlay);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  // ================================================================================
  // Section - methods called when user interacts with the application
  // ================================================================================

  @Override
  public void onUserConnect(String ipAddress, int port) {
    Request request = new Request(RequestType.CONNECT);
    request.addParameter(ipAddress, Constants.IP_ADDRESS_PARAMETER);
    request.addParameter(port, Constants.PORT_PARAMETER);
    network.sendRequest(request);
  }

  @Override
  public void userConnect() {
    onUserConnect(ipAddress, port);
  }

  @Override
  public void userLogOut() {
    network.closeConnection();
    showScene(SceneType.CONNECT);
    sceneOverlayController.setNick("");
    sceneOverlayController.setIpAddress("");
    connectController.setConnected(false);
    connectController.cleanConnectionLabel();
    userNick = "";

  }

  @Override
  public void onUserLogin(String nick) {
    userNick = "";
    Request request = new Request(RequestType.LOGIN);
    request.addParameter(nick, Constants.NICK_PARAMETER);
    network.sendRequest(request);
  }

  @Override
  public void onUserCreateNewRoom() {
    network.sendRequest(RequestType.ROOM_CREATE);
  }

  @Override
  public void onUserEnterRoom(int id) {
    Request request = new Request(RequestType.ROOM_ENTER);
    request.addParameter(id, Constants.ROOM_ID_PARAMETER);
    network.sendRequest(request);
  }

  @Override
  public void userLeaveRoom(int id) {
    Request request = new Request(RequestType.ROOM_LEAVE);
    request.addParameter(id, Constants.ROOM_ID_PARAMETER);
    network.sendRequest(request);
  }

  @Override
  public void userDeleteRoom(int id) {
    Request request = new Request(RequestType.ROOM_DELETE);
    request.addParameter(id, Constants.ROOM_ID_PARAMETER);
    network.sendRequest(request);
  }

  @Override
  public void userStartRoom(int id) {
    Request request = new Request(RequestType.ROOM_START);
    request.addParameter(id, Constants.ROOM_ID_PARAMETER);
    network.sendRequest(request);
  }

  @Override
  public void onUserRevealCard(int cardID) {
    Request request = new Request(RequestType.REVEAL_CARD);
    request.addParameter(cardID, Constants.CARD_ID_PARAMETER);
    network.sendRequest(request);
  }

  @Override
  public void onUserBackToLobby() {
    showScene(SceneType.LOBBY);
  }

  // ================================================================================
  // Section - methods called when received message from server or connection
  // to server
  // ================================================================================

  @Override
  public void onServerMessageRoomList(ArrayList<Room> rooms) {
    if (currentScene == SceneType.END_GAME) {
      lobbyController.setRooms(rooms);
      return;
    }

    lobbyController.setRooms(rooms);

    Room userRoom = null;
    for (Room room : rooms) {
      if (room.getPlayers().contains(userNick)) {
        userRoom = room;
      }
    }

    if (userRoom == null) {
      showScene(SceneType.LOBBY);
    } else {
      showScene(SceneType.ROOM);
      roomController.setRoom(userRoom);
    }
  }

  @Override
  public void onServerMessageStartGame(int[] board) {
    if (currentScene != SceneType.ROOM) {
      sceneOverlayController.setMessage("Received start game at invalid time");
      return;
    }
    showScene(SceneType.GAME);
    gameController.setBoard(board);
    ArrayList<Player> players = new ArrayList<Player>();
    roomController.getPlayers().forEach(player -> players.add(new Player(player)));
    gameController.setPlayers(players);
  }

  @Override
  public void onServerMessagePlayersTurn(String nick) {
    if (currentScene != SceneType.GAME) {
      sceneOverlayController.setMessage("Received players turn at invalid time");
      return;
    }
    gameController.setPlayersTurn(nick, true);
  }

  @Override
  public void onServerMessageRevealCard(int cardId) {
    if (currentScene != SceneType.GAME) {
      sceneOverlayController.setMessage("Received reveal card at invalid time");
      return;
    }
    gameController.revealCard(cardId);
  }

  @Override
  public void onServerMessagePlayerConnectionStatus(ConnectionStatusType connectionStatus, String nick) {
    if (currentScene != SceneType.GAME) {
      sceneOverlayController.setMessage("Received player connection status at invalid time");
      return;
    }
    gameController.setPlayersConnectionStatus(connectionStatus, nick);
  }

  @Override
  public void onServerMessageGameEnd() {
    if (currentScene != SceneType.GAME) {
      sceneOverlayController.setMessage("Received game end at invalid time");
      return;
    }
    gameController.finishTurn();
    showScene(SceneType.END_GAME);
    endGameController.setPlayers(gameController.getPlayers());
  }

  @Override
  public void onServerGameStatus(int[] board, String playerOnTurn, ArrayList<Player> players) {
    showScene(SceneType.GAME);
    gameController.setBoard(board);
    gameController.setPlayers(players);
    gameController.setPlayersTurn(playerOnTurn, false);
  }

  @Override
  public void onServerMessageInvalid(String message) {
    sceneOverlayController.setMessage("Invalid message received: " + message);
  }

  @Override
  public void onServerConnectionChanged(ConnectionStatusType connectionStatus) {
    boolean disableConnectionButton = false;
    if (currentScene == SceneType.CONNECT && connectionStatus == ConnectionStatusType.DISCONNECTED) {
      connectController.setConnected(false);
      disableConnectionButton = true;
      connectController.cleanConnectionLabel();
    }
    if (currentScene == SceneType.GAME) {
      gameController.setPlayersConnectionStatus(connectionStatus, userNick);
    }
    sceneOverlayController.setConnectionStatus(connectionStatus, disableConnectionButton);

  }

  @Override
  public void onServerResponse(Request sentRequest, int responseCode, String message) {
    sceneOverlayController.setMessage("");

    if (responseCode <= 0 || responseCode > Constants.RESPONSE_FAIL) {
      onServerMessageInvalid("Unknown response code " + responseCode);
    }

    if (responseCode == Constants.RESPONSE_FAIL)
      sceneOverlayController.setMessage("Request failed: " + message);

    RequestType requestType = sentRequest.getRequestType();

    switch (requestType) {
      case CONNECT: {
        switch (responseCode) {
          case Constants.RESPONSE_SUCCESS: {
            connectController.setConnected(true);
            String ipAddress = sentRequest.getStringParameter(Constants.IP_ADDRESS_PARAMETER);
            int port = sentRequest.getIntParameter(Constants.PORT_PARAMETER);
            sceneOverlayController.setIpAddress(ipAddress);
            this.ipAddress = ipAddress;
            this.port = port;
            if (!userNick.equals("")) {
              onUserLogin(userNick);
            }
            break;
          }
          case Constants.RESPONSE_FAIL: {
            connectController.setConnected(false);
            break;
          }
        }
        break;
      }

      case LOGIN: {
        switch (responseCode) {
          case Constants.RESPONSE_SUCCESS: {
            showScene(SceneType.LOBBY);
            userNick = sentRequest.getStringParameter(Constants.NICK_PARAMETER);
            sceneOverlayController.setNick(userNick);
            break;
          }
          case Constants.RESPONSE_FAIL: {
            connectController.setLoginFailed();
            break;
          }
        }
        break;
      }

      case ROOM_ENTER: {
        switch (responseCode) {
          case Constants.RESPONSE_SUCCESS: {
            showScene(SceneType.ROOM);
            int roomId = sentRequest.getIntParameter(Constants.ROOM_ID_PARAMETER);
            Room room = lobbyController.getRooms().stream().filter(r -> r.getId() == roomId).findFirst().get();
            room.getPlayers().add(userNick);
            roomController.setRoom(room);
            break;
          }
        }
        break;
      }
      case ROOM_CREATE: {
        switch (responseCode) {
          case Constants.RESPONSE_SUCCESS: {
            try {
              int id = Integer.parseInt(message);
              lobbyController.addRoom(new Room(id, userNick, new ArrayList<String>()));
            } catch (Exception e) {
              onServerMessageInvalid("unknown room id \"" + message + "\"");
            }
            break;
          }
        }
        break;
      }

      case ROOM_DELETE: {
        switch (responseCode) {
          case Constants.RESPONSE_SUCCESS: {
            showScene(SceneType.LOBBY);
            int roomId = sentRequest.getIntParameter(Constants.ROOM_ID_PARAMETER);
            lobbyController.deleteRoom(roomId);
            break;
          }
        }
        break;
      }

      case ROOM_LEAVE: {
        switch (responseCode) {
          case Constants.RESPONSE_SUCCESS: {
            roomController.getRoom().getPlayers().remove(userNick);
            lobbyController.refresh();
            showScene(SceneType.LOBBY);
            break;
          }
        }
        break;
      }

      case ROOM_START: {
        switch (responseCode) {
          case Constants.RESPONSE_SUCCESS: {
            sceneOverlayController.setMessage("Starting room");
            break;
          }
        }
        break;
      }

      case REVEAL_CARD:
        switch (responseCode) {
          case Constants.RESPONSE_SUCCESS: {
            int cardId = sentRequest.getIntParameter(Constants.CARD_ID_PARAMETER);
            gameController.revealCard(cardId);
            break;
          }
        }
        break;
      default:
        break;

    }
  }
}
