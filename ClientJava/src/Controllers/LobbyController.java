package Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;

import ListenerInterfaces.ILobbyRoomButtonListener;
import ListenerInterfaces.ILobbySceneListener;
import Objects.Room;
import Constants.Constants;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class LobbyController implements ILobbyRoomButtonListener {

    private ILobbySceneListener listener;
    private ArrayList<Room> rooms;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button CreateRoomButton;

    @FXML
    private VBox roomListVBox;

    public LobbyController(ILobbySceneListener listener) {
        this.listener = listener;
    }

    @FXML
    void initialize() {

    }

    @FXML
    private void CreateRoomPressed(ActionEvent event) {
        listener.onUserCreateNewRoom();
    }

    @Override
    public void onButtonPressed(int id) {
        listener.onUserEnterRoom(id);
    }

    public void setRooms(ArrayList<Room> rooms) {
        this.rooms = rooms;
        refresh();
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

    public void deleteRoom(int roomId) {
        rooms.removeIf(r -> r.getId() == roomId);
        refresh();
    }

    public void refresh() {
        roomListVBox.getChildren().clear();
        for (Room room : rooms) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(Constants.LOBBY_ROOM_BUTTON_VIEW_LOCATION));
            LobbyRoomButtonController lobbyRoomButtonController = new LobbyRoomButtonController(room, this);
            loader.setController(lobbyRoomButtonController);
            try {
                roomListVBox.getChildren().add(loader.load());
            } catch (IOException e) {
                return;
            }
        }
    }

    public void addRoom(Room room) {
        rooms.add(room);
        Collections.sort(rooms);
        refresh();
    }

}
