package Controllers;

import java.util.ArrayList;

import Constants.Constants;
import ListenerInterfaces.IRoomSceneListener;
import Objects.Room;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

public class RoomController {

    private String userNick;
    private Room room;
    private IRoomSceneListener listener;

    @FXML
    private Label roomNameLabel;

    @FXML
    private Label roomOwnerLabel;

    @FXML
    private Label playersCountLabel;

    @FXML
    private Button leaveRoomButton;

    @FXML
    private Button startRoomButton;

    @FXML
    private Button deleteRoomButton;

    @FXML
    private ListView<String> playersListView;

    @FXML
    void DeleteRoomPressed(ActionEvent event) {
        listener.userDeleteRoom(room.getId());
    }

    @FXML
    void LeaveRoomPressed(ActionEvent event) {
        listener.userLeaveRoom(room.getId());
    }

    @FXML
    void StartRoomPressed(ActionEvent event) {
        listener.userStartRoom(room.getId());
    }

    @FXML
    void initialize() {

    }

    public RoomController(IRoomSceneListener listener, String userNick) {
        this.listener = listener;
        this.userNick = userNick;
    }

    public void setRoom(Room room) {
        this.room = room;
        refresh();
    }

    public Room getRoom() {
        return room;
    }

    private void refresh() {
        roomNameLabel.setText("Room " + room.getId());
        boolean userOwner = room.getOwner().equals(userNick);
        if (userOwner) {
            roomOwnerLabel.setText("You are the owner");
            deleteRoomButton.setDisable(false);
            if (room.getPlayers().size() >= Constants.MIN_NUMBER_OF_PLAYERS) {
                startRoomButton.setDisable(false);
            } else {
                startRoomButton.setDisable(true);
            }
        } else {
            roomOwnerLabel.setText("Owner: " + room.getOwner());
            startRoomButton.setDisable(true);
            deleteRoomButton.setDisable(true);
        }
        playersCountLabel.setText(room.getPlayersCount() + "/" + Constants.MAX_NUMBER_OF_PLAYERS);

        playersListView.getItems().clear();
        room.getPlayers().forEach(player -> playersListView.getItems().add(player));
    }

    public ArrayList<String> getPlayers() {
        return room.getPlayers();
    }
}
