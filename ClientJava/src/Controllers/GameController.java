package Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.ResourceBundle;

import ListenerInterfaces.ICardPanelListener;
import ListenerInterfaces.IGameSceneListener;
import Objects.ConnectionStatusType;
import Objects.Player;
import Constants.Constants;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.util.Duration;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;

public class GameController implements ICardPanelListener {

    private IGameSceneListener listener;
    private String userNick;
    private Hashtable<String, PlayerPanelController> playersPanelControllers = new Hashtable<String, PlayerPanelController>();
    private Hashtable<Integer, CardController> cardControllers = new Hashtable<Integer, CardController>();
    private ArrayList<CardController> temporaryRevealedCards = new ArrayList<CardController>();
    private IntegerProperty timeSeconds = new SimpleIntegerProperty(Constants.GAME_ROUND_TIME);
    private Timeline timeline;
    private String playersTurn = "";

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label playersTurnLabel;

    @FXML
    private Label timeOfRoundLabel;

    @FXML
    private GridPane boardGridPane;

    @FXML
    private HBox playersListHBox;

    @FXML
    void initialize() {
        timeOfRoundLabel.textProperty().bind(timeSeconds.asString());
        boardGridPane.setDisable(true);
    }

    public GameController(IGameSceneListener listener, String userNick) {
        this.listener = listener;
        this.userNick = userNick;
    }

    public void setBoard(int[] board) {
        boardGridPane.getChildren().clear();
        int rows = boardGridPane.getRowCount();
        int columns = boardGridPane.getColumnCount();

        int cardId = 0;
        for (int rowIndex = 0; rowIndex < rows; rowIndex++) {
            for (int columnIndex = 0; columnIndex < columns; columnIndex++) {
                if (board[cardId] == -1) {
                    cardId++;
                    continue;
                }
                FXMLLoader loader = new FXMLLoader(getClass().getResource(Constants.CARD_PANEL_VIEW_LOCATION));
                CardController controller = new CardController(this, cardId, board[cardId]);
                loader.setController(controller);
                Parent element = null;
                try {
                    element = loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                boardGridPane.add(element, columnIndex, rowIndex);
                cardControllers.put(cardId, controller);
                cardId++;
            }
        }
    }

    public void setPlayers(ArrayList<Player> players) {
        playersListHBox.getChildren().clear();
        playersPanelControllers.clear();
        int index = 0;
        for (Player player : players) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(Constants.PLAYER_PANEL_VIEW_LOCATION));
            PlayerPanelController controller = new PlayerPanelController(player);
            loader.setController(controller);
            playersPanelControllers.put(player.getNick(), controller);
            Parent element = null;
            try {
                element = loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            HBox.setHgrow(element, Priority.ALWAYS);
            playersListHBox.getChildren().add(element);
            if (index + 1 < players.size()) {
                playersListHBox.getChildren().add(new Separator(Orientation.VERTICAL));
            }
            index++;
        }
    }

    public void setPlayersTurn(String nick, boolean startTimer) {
        if (!playersTurn.equals(""))
            finishTurn();

        playersTurn = nick;

        if (startTimer) {
            startTimer();
        }

        boolean usersTurn = nick.equals(userNick);
        if (usersTurn) {
            playersTurnLabel.setText("YOUR TURN!");
            boardGridPane.setDisable(false);
        } else {
            playersTurnLabel.setText("PLAYING: " + nick);
            boardGridPane.setDisable(true);
        }
        playersPanelControllers.values().forEach(playerPanel -> playerPanel.setHighlited(false));
        playersPanelControllers.get(nick).setHighlited(true);
    }

    public void finishTurn() {
        boolean playerScored = temporaryRevealedCards.size() > 1
                && temporaryRevealedCards.get(0).getCardType() == temporaryRevealedCards.get(1).getCardType();
        if (playerScored) {
            temporaryRevealedCards.forEach(card -> card.remove());
            increasePlayersScore(playersTurn);
        } else {
            temporaryRevealedCards.forEach(card -> card.hide());
        }
        temporaryRevealedCards.clear();
    }

    public void startTimer() {

        if (timeline != null) {
            timeline.stop();
        }
        timeSeconds.set(Constants.GAME_ROUND_TIME - 1);
        timeline = new Timeline();
        timeline.getKeyFrames()
                .add(new KeyFrame(Duration.seconds(Constants.GAME_ROUND_TIME), new KeyValue(timeSeconds, 0)));
        timeline.playFromStart();
    }

    public void setPlayersConnectionStatus(ConnectionStatusType connectionStatus, String nick) {
        playersPanelControllers.get(nick).setConnectionStatus(connectionStatus);
    }

    public void increasePlayersScore(String nick) {
        playersPanelControllers.get(nick).increaseScore();
    }

    public void revealCard(int cardId) {
        if (temporaryRevealedCards.size() >= 1) {
            boardGridPane.setDisable(true);
        }
        CardController cardController = cardControllers.get(cardId);
        temporaryRevealedCards.add(cardController);
        cardController.reveal();
    }

    @Override
    public void onCardPressed(int cardId, int cardType) {
        listener.onUserRevealCard(cardId);
    }

    public ArrayList<Player> getPlayers() {
        ArrayList<Player> players = new ArrayList<Player>();
        playersPanelControllers.values().forEach(p -> players.add(p.getPlayer()));
        Collections.sort(players);
        return players;
    }

}
