package Controllers;

import java.net.URL;
import java.util.ResourceBundle;

import ListenerInterfaces.ISceneOverlayListener;
import Objects.ConnectionStatusType;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class SceneOverlayController {

    private ISceneOverlayListener listener;

    enum ButtonType {
        LOGOUT, CONNECT
    }

    ButtonType buttonType = ButtonType.LOGOUT;

    @FXML
    private ResourceBundle resources;

    @FXML
    private Pane applicationContentPane;

    @FXML
    private URL location;

    @FXML
    private Label connectionLabel;

    @FXML
    private Label nameLabel;

    @FXML
    private Label ipAddressLabel;

    @FXML
    private Label messageLabel;

    @FXML
    private VBox connectionButton;

    @FXML
    private Label connectionButtonLabel;

    @FXML
    void initialize() {

    }

    public SceneOverlayController(ISceneOverlayListener listener) {
        this.listener = listener;
    }

    public void setScene(Parent scene) {
        applicationContentPane.getChildren().clear();
        applicationContentPane.getChildren().add(scene);
    }

    public void setNick(String name) {
        nameLabel.setText(name);
    }

    public void setIpAddress(String ipAddress) {
        if (ipAddress.equals("")) {
            connectionLabel.setText("");
        }
        ipAddressLabel.setText(ipAddress);
    }

    public void setMessage(String message) {
        messageLabel.setText(message);
    }

    public void setConnectionStatus(ConnectionStatusType connectionStatus, boolean disableActionButton) {
        connectionButton.setDisable(disableActionButton);
        switch (connectionStatus) {
            case CONNECTED: {
                connectionButtonLabel.setText("Logout");
                buttonType = ButtonType.LOGOUT;
                connectionLabel.setText("Connected");
                connectionLabel.getStyleClass().removeAll("warning", "danger");
                connectionLabel.getStyleClass().add("success");
                return;
            }
            case DISCONNECTED: {
                connectionButtonLabel.setText("Connect");
                buttonType = ButtonType.CONNECT;
                connectionLabel.setText("Disconnected");
                connectionLabel.getStyleClass().removeAll("warning", "success");
                connectionLabel.getStyleClass().add("danger");
                return;
            }

            case PENDING: {
                connectionButtonLabel.setText("Logout");
                buttonType = ButtonType.LOGOUT;
                connectionLabel.setText("Pending...");
                connectionLabel.getStyleClass().removeAll("success", "danger");
                connectionLabel.getStyleClass().add("warning");
                return;
            }

        }
    }

    @FXML
    void onConnectionButtonPressed(MouseEvent event) {
        if (buttonType == ButtonType.LOGOUT) {
            listener.userLogOut();
        } else if (buttonType == ButtonType.CONNECT) {
            listener.userConnect();
        }
    }
}
