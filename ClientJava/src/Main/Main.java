package Main;

import Controllers.MainController;
import Network.Network;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Main class containing the main method
 */
public class Main extends Application {

    /**
     * enter point of application
     * 
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage window) throws Exception {
        Network network = new Network();
        new MainController(window, network);
    }

}
