package Constants;

import Objects.Parameter;

public class Constants {
    public static final String STAGE_TITLE = "PEXESO";
    public static final String VERSION = "1.0";

    public static final String CONNECT_VIEW_LOCATION = "/Resources/Views/ConnectView.fxml";
    public static final String LOBBY_VIEW_LOCATION = "/Resources/Views/LobbyView.fxml";
    public static final String ROOM_VIEW_LOCATION = "/Resources/Views/RoomView.fxml";
    public static final String GAME_VIEW_LOCATION = "/Resources/Views/GameView.fxml";
    public static final String END_GAME_VIEW_LOCATION = "/Resources/Views/EndGameView.fxml";
    public static final String SCENE_OVERLAY_VIEW_LOCATION = "/Resources/Views/SceneOverlayView.fxml";
    public static final String LOBBY_ROOM_BUTTON_VIEW_LOCATION = "/Resources/Views/LobbyRoomButtonView.fxml";
    public static final String PLAYER_PANEL_VIEW_LOCATION = "/Resources/Views/PlayerPanelView.fxml";
    public static final String CARD_PANEL_VIEW_LOCATION = "/Resources/Views/CardPanelView.fxml";
    public static final String CARD_IMAGE_LOCATION = "/Resources/Cards/";

    public static final int MINIMAL_NICK_LENGTH = 4;
    public static final int MAXIMAL_NICK_LENGTH = 12;

    public static final int BOARD_SIZE = 32;
    public static final int MAX_NUMBER_OF_PLAYERS = 10;
    public static final int MIN_NUMBER_OF_PLAYERS = 2;
    public static final int GAME_ROUND_TIME = 8;

    public static final int CONNECT_TIMEOUT = 5000;
    public static final int RESPONSE_TIMEOUT = 1000;
    public static final int PING_STANDARD_INTERVAL = 5000;
    public static final int PING_LOST_CONNECTION_INTERVAL = 3000;
    public static final int DISCONNECT_INTERVAl = 20000;

    public static final int PING_STANDARD = 1;
    public static final int PING_LOST_CONNECTION = 2;

    public static final String MESSAGE_PROTOCOL_IDENTIFICATOR = "KIVUPS";

    public static final int MESSAGE_TYPE_LENGTH = 4;
    public static final String REQUEST_LOGIN = "lgin";
    public static final String REQUEST_REVEAL_CARD = "rvcc";
    public static final String REQUEST_ROOM_CREATE = "rmcr";
    public static final String REQUEST_ROOM_DELETE = "rmdl";
    public static final String REQUEST_ROOM_ENTER = "rmen";
    public static final String REQUEST_ROOM_LEAVE = "rmlv";
    public static final String REQUEST_ROOM_START = "rmst";

    public static final String REQUEST_PING = "ping";

    public static final String SERVER_RESPONSE = "resp";
    public static final String SERVER_ROOM_LIST_MESSAGE = "list";
    public static final String SERVER_REVEAL_CARD_MESSAGE = "rvcs";
    public static final String SERVER_PLAYERS_TURN_MESSAGE = "ptrn";
    public static final String SERVER_PLAYERS_STATUS_MESSAGE = "psts";
    public static final String SERVER_GAME_END_MESSAGE = "gend";
    public static final String SERVER_GAME_START_MESSAGE = "gstr";
    public static final String SERVER_GAME_STATUS_MESSAGE = "gsts";

    public static final int RESPONSE_SUCCESS = 1;
    public static final int RESPONSE_FAIL = 2;

    public static final int MESSAGE_CONNECTED = 1;
    public static final int MESSAGE_DISCONNECTED = 2;
    public static final int MESSAGE_PENDING = 3;

    public static final int MESSAGE_LENGTH_DECIMALS = 4;
    public static final int MESSAGE_RESPONSE_CODE_DECIMALS = 2;
    public static final int MESSAGE_CONNECTION_DECIMALS = 1;
    public static final int MESSAGE_RESPONSE_MESSAGE_DECIMALS = 2;
    public static final int ROOM_LIST_DECIMALS = 4;
    public static final int PLAYERS_LIST_DECIMALS = 3;
    public static final int PLAYERS_STATUS_LIST_DECIMALS = 3;
    public static final int PLAYERS_SCORE_DECIMALS = 2;
    public static final int CARD_TYPE_DECIMALS = 2;

    public static final Parameter IP_ADDRESS_PARAMETER = new Parameter("IP_ADDRESS", -1);
    public static final Parameter PORT_PARAMETER = new Parameter("PORT", -1);
    public static final Parameter CARD_ID_PARAMETER = new Parameter("CARD_ID", 2);
    public static final Parameter ROOM_ID_PARAMETER = new Parameter("ROOM_ID", 2);
    public static final Parameter NICK_PARAMETER = new Parameter("NICK", 2);
    public static final Parameter BOARD_PARAMETER = new Parameter("BOARD", 2);
    public static final Parameter PING_TYPE = new Parameter("PING_TYPE", 1);

}
