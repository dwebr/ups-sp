package Network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;

import Constants.Constants;

import ListenerInterfaces.INetworkMessageReceiverListener;
import Objects.ConnectionStatusType;
import Objects.Player;
import Objects.Room;
import javafx.application.Platform;

public class NetworkMessageReceiver {
    Socket socket;
    INetworkMessageReceiverListener listener;
    BufferedReader reader;

    public NetworkMessageReceiver(Socket socket, INetworkMessageReceiverListener listener) {
        this.socket = socket;
        this.listener = listener;
        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            receiveMessages();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        try {
            if (reader != null) {
                reader.close();
                reader = null;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void receiveMessages() {
        Runnable runnable = () -> {
            while (true && reader != null) {
                try {
                    String message = reader.readLine();
                    if (message == null) {
                        Platform.runLater(() -> listener.onConnectionClosed());
                        return;
                    }

                    System.out.println("Received message: " + message);
                    Platform.runLater(() -> readMessage(message));

                } catch (Exception e) {
                    Platform.runLater(() -> listener.onConnectionClosed());
                    return;
                }
            }
        };

        Thread thread = new Thread(runnable);
        thread.setDaemon(true);
        thread.start();
    }

    public void readMessage(String message) {
        if (message.length() < Constants.MESSAGE_PROTOCOL_IDENTIFICATOR.length() + Constants.MESSAGE_TYPE_LENGTH
                + Constants.MESSAGE_LENGTH_DECIMALS) {
            listener.onInvalidMessageReceived("invalid header length " + message.length());
            return;
        }

        boolean startsWithMagic = message.startsWith(Constants.MESSAGE_PROTOCOL_IDENTIFICATOR);
        if (!startsWithMagic) {
            listener.onInvalidMessageReceived("invalid protocol identificator \""
                    + message.substring(0, Constants.MESSAGE_PROTOCOL_IDENTIFICATOR.length()) + "\"");
            return;
        }
        message = message.substring(Constants.MESSAGE_PROTOCOL_IDENTIFICATOR.length());

        String messageType = message.substring(0, Constants.MESSAGE_TYPE_LENGTH);
        message = message.substring(Constants.MESSAGE_TYPE_LENGTH);
        int messageLength;
        try {
            messageLength = Integer.valueOf(message.substring(0, Constants.MESSAGE_LENGTH_DECIMALS));
        } catch (NumberFormatException e) {
            listener.onInvalidMessageReceived("invalid header ");
            return;
        }

        message = message.substring(Constants.MESSAGE_TYPE_LENGTH);
        if (messageLength != message.length()) {
            listener.onInvalidMessageReceived(
                    "expected body length: " + messageLength + ", received: " + message.length());
            return;
        }
        readMessageBody(messageType, message);
    }

    public void readMessageBody(String messageType, String messageBody) {
        switch (messageType) {
            case Constants.SERVER_RESPONSE: {
                readResponse(messageBody);
                return;
            }
            case Constants.SERVER_GAME_END_MESSAGE: {
                readGameEndMessage(messageBody);
                return;
            }
            case Constants.SERVER_GAME_START_MESSAGE: {
                readGameStartMessage(messageBody);
                return;
            }
            case Constants.SERVER_GAME_STATUS_MESSAGE: {
                readGameStatusMessage(messageBody);
                return;
            }
            case Constants.SERVER_PLAYERS_STATUS_MESSAGE: {
                readPlayersStatusMessage(messageBody);
                return;
            }
            case Constants.SERVER_PLAYERS_TURN_MESSAGE: {
                readServerPlayersTurnMessage(messageBody);
                return;
            }
            case Constants.SERVER_REVEAL_CARD_MESSAGE: {
                readRevealCardMessage(messageBody);
                return;
            }
            case Constants.SERVER_ROOM_LIST_MESSAGE: {
                readRoomListMessage(messageBody);
                return;
            }
            default: {
                listener.onInvalidMessageReceived("unknown message type \"" + messageType + "\"");
            }
        }

    }

    private void readRoomListMessage(String messageBody) {
        if (messageBody.length() < Constants.ROOM_LIST_DECIMALS) {
            listener.onInvalidMessageReceived("invalid room list format");
        }

        int roomListLength;
        try {
            roomListLength = Integer.valueOf(messageBody.substring(0, Constants.ROOM_LIST_DECIMALS));
        } catch (NumberFormatException e) {
            listener.onInvalidMessageReceived("invalid room list format");
            return;
        }
        messageBody = messageBody.substring(Constants.ROOM_LIST_DECIMALS);
        if (roomListLength != messageBody.length()) {
            listener.onInvalidMessageReceived("invalid room list format");
        }

        ArrayList<Room> rooms = new ArrayList<Room>();
        int charactersRead = 0;
        while (charactersRead < roomListLength) {
            int roomId;
            try {
                roomId = Integer.valueOf(messageBody.substring(0, Constants.ROOM_ID_PARAMETER.getLength()));
                messageBody = messageBody.substring(Constants.ROOM_ID_PARAMETER.getLength());
                charactersRead += Constants.ROOM_ID_PARAMETER.getLength();
            } catch (Exception e) {
                listener.onInvalidMessageReceived("invalid room list format");
                return;
            }

            String roomOwner;
            try {
                int roomOwnerLength = Integer.valueOf(messageBody.substring(0, Constants.NICK_PARAMETER.getLength()));
                messageBody = messageBody.substring(Constants.NICK_PARAMETER.getLength());
                charactersRead += Constants.NICK_PARAMETER.getLength();
                roomOwner = messageBody.substring(0, roomOwnerLength);
                messageBody = messageBody.substring(roomOwnerLength);
                charactersRead += roomOwnerLength;
            } catch (Exception e) {
                listener.onInvalidMessageReceived("invalid room list format");
                return;
            }

            int playersArrayLength;
            try {
                playersArrayLength = Integer.valueOf(messageBody.substring(0, Constants.PLAYERS_LIST_DECIMALS));
                messageBody = messageBody.substring(Constants.PLAYERS_LIST_DECIMALS);
                charactersRead += Constants.PLAYERS_LIST_DECIMALS;
            } catch (Exception e) {
                listener.onInvalidMessageReceived("invalid room list format");
                return;
            }

            ArrayList<String> players = new ArrayList<String>();
            int playersArrayCharactersRead = 0;
            while (playersArrayCharactersRead < playersArrayLength) {
                String player;
                try {
                    int playerLength = Integer.valueOf(messageBody.substring(0, Constants.NICK_PARAMETER.getLength()));
                    messageBody = messageBody.substring(Constants.NICK_PARAMETER.getLength());
                    playersArrayCharactersRead += Constants.NICK_PARAMETER.getLength();
                    player = messageBody.substring(0, playerLength);
                    messageBody = messageBody.substring(playerLength);
                    playersArrayCharactersRead += playerLength;
                } catch (Exception e) {
                    listener.onInvalidMessageReceived("invalid room list format");
                    return;
                }
                players.add(player);
            }
            charactersRead += playersArrayCharactersRead;
            rooms.add(new Room(roomId, roomOwner, players));
        }

        listener.onRoomListReceived(rooms);
    }

    private void readRevealCardMessage(String messageBody) {
        try {
            int cardId = Integer.parseInt(messageBody.substring(0, Constants.CARD_ID_PARAMETER.getLength()));
            listener.onRevealCardReceived(cardId);
        } catch (Exception e) {
            listener.onInvalidMessageReceived("invalid card id");
            return;
        }
    }

    private void readPlayersStatusMessage(String messageBody) {
        int statusId;
        try {
            statusId = Integer.parseInt(messageBody.substring(0, Constants.MESSAGE_CONNECTION_DECIMALS));
            messageBody = messageBody.substring(Constants.MESSAGE_CONNECTION_DECIMALS);
        } catch (Exception e) {
            listener.onInvalidMessageReceived("invalid player status");
            return;
        }

        String player;
        try {
            int playerLength = Integer.valueOf(messageBody.substring(0, Constants.NICK_PARAMETER.getLength()));
            messageBody = messageBody.substring(Constants.NICK_PARAMETER.getLength());
            player = messageBody.substring(0, playerLength);
            messageBody = messageBody.substring(playerLength);
        } catch (Exception e) {
            listener.onInvalidMessageReceived("invalid player nic");
            return;
        }

        listener.onPlayerConnectionStatusReceived(intToConnectionStatus(statusId), player);
    }

    private void readServerPlayersTurnMessage(String messageBody) {

        try {
            int nickLength = Integer.parseInt(messageBody.substring(0, Constants.NICK_PARAMETER.getLength()));
            messageBody = messageBody.substring(Constants.NICK_PARAMETER.getLength());
            if (nickLength != messageBody.length()) {
                listener.onInvalidMessageReceived("invalid players turn nick");
                return;
            }

        } catch (Exception e) {
            listener.onInvalidMessageReceived("invalid players turn nick");
            return;
        }

        listener.onPlayersTurnReceived(messageBody);
    }

    private void readGameStatusMessage(String messageBody) {
        int[] board = new int[Constants.BOARD_SIZE];
        String playerOnTurn;
        ArrayList<Player> players = new ArrayList<Player>();

        for (int i = 0; i < Constants.BOARD_SIZE; i++) {
            try {
                int cardType = Integer.parseInt(messageBody.substring(0, Constants.CARD_TYPE_DECIMALS));
                messageBody = messageBody.substring(Constants.CARD_TYPE_DECIMALS);
                board[i] = cardType;
            } catch (Exception e) {
                listener.onInvalidMessageReceived("invalid board format");
                return;
            }
        }

        int playerOnTurnLength = Integer.valueOf(messageBody.substring(0, Constants.NICK_PARAMETER.getLength()));
        messageBody = messageBody.substring(Constants.NICK_PARAMETER.getLength());

        playerOnTurn = messageBody.substring(0, playerOnTurnLength);
        messageBody = messageBody.substring(playerOnTurnLength);

        int playersArrayLength;
        try {
            playersArrayLength = Integer.valueOf(messageBody.substring(0, Constants.PLAYERS_STATUS_LIST_DECIMALS));
            messageBody = messageBody.substring(Constants.PLAYERS_STATUS_LIST_DECIMALS);
        } catch (Exception e) {
            listener.onInvalidMessageReceived("invalid players status");
            return;
        }

        int playersArrayCharactersRead = 0;
        while (playersArrayCharactersRead < playersArrayLength) {
            try {
                int playerNickLength = Integer.valueOf(messageBody.substring(0, Constants.NICK_PARAMETER.getLength()));
                messageBody = messageBody.substring(Constants.NICK_PARAMETER.getLength());
                playersArrayCharactersRead += Constants.NICK_PARAMETER.getLength();

                String nick = messageBody.substring(0, playerNickLength);
                messageBody = messageBody.substring(playerNickLength);
                playersArrayCharactersRead += playerNickLength;

                ConnectionStatusType status = intToConnectionStatus(
                        Integer.valueOf(messageBody.substring(0, Constants.MESSAGE_CONNECTION_DECIMALS)));
                messageBody = messageBody.substring(Constants.MESSAGE_CONNECTION_DECIMALS);
                playersArrayCharactersRead += Constants.MESSAGE_CONNECTION_DECIMALS;

                int score = Integer.valueOf(messageBody.substring(0, Constants.PLAYERS_SCORE_DECIMALS));
                messageBody = messageBody.substring(Constants.PLAYERS_SCORE_DECIMALS);
                playersArrayCharactersRead += Constants.PLAYERS_SCORE_DECIMALS;

                players.add(new Player(nick, score, status));

            } catch (Exception e) {
                listener.onInvalidMessageReceived("invalid room list format");
                return;
            }

        }

        listener.onGameStatusReceived(board, playerOnTurn, players);
    }

    private void readGameStartMessage(String messageBody) {
        int[] board = new int[Constants.BOARD_SIZE];

        if (messageBody.length() != Constants.CARD_TYPE_DECIMALS * Constants.BOARD_SIZE) {
            listener.onInvalidMessageReceived("invalid board");
            return;
        }

        for (int i = 0; i < Constants.BOARD_SIZE; i++) {
            try {
                int cardType = Integer.parseInt(messageBody.substring(0, Constants.CARD_TYPE_DECIMALS));
                messageBody = messageBody.substring(Constants.CARD_TYPE_DECIMALS);
                board[i] = cardType;
            } catch (Exception e) {
                listener.onInvalidMessageReceived("invalid board format");
                return;
            }
        }

        listener.onStartGameReceived(board);
    }

    private void readGameEndMessage(String messageBody) {
        listener.onGameEndReceived();
    }

    private void readResponse(String messageBody) {
        int responseCode;
        try {
            responseCode = Integer.parseInt(messageBody.substring(0, Constants.MESSAGE_RESPONSE_CODE_DECIMALS));
            messageBody = messageBody.substring(Constants.MESSAGE_RESPONSE_CODE_DECIMALS);

        } catch (Exception e) {
            listener.onInvalidMessageReceived("invalid response code \"" + messageBody + "\"");
            return;
        }

        try {
            int responseMessageLength = Integer
                    .parseInt(messageBody.substring(0, Constants.MESSAGE_RESPONSE_MESSAGE_DECIMALS));
            messageBody = messageBody.substring(Constants.MESSAGE_RESPONSE_MESSAGE_DECIMALS);
            if (responseMessageLength != messageBody.length()) {
                listener.onInvalidMessageReceived("invalid response message");
                return;
            }

        } catch (Exception e) {
            listener.onInvalidMessageReceived("invalid response message");
            return;
        }

        listener.onResponseReceived(responseCode, messageBody);
    }

    private ConnectionStatusType intToConnectionStatus(int code) {
        switch (code) {
            case Constants.MESSAGE_CONNECTED: {
                return ConnectionStatusType.CONNECTED;
            }
            case Constants.MESSAGE_DISCONNECTED: {
                return ConnectionStatusType.DISCONNECTED;
            }
            case Constants.MESSAGE_PENDING: {
                return ConnectionStatusType.PENDING;
            }
            default: {
                return ConnectionStatusType.DISCONNECTED;
            }
        }
    }
}
