package Network;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import Constants.Constants;
import ListenerInterfaces.INetworkListener;
import ListenerInterfaces.INetworkMessageReceiverListener;
import Objects.ConnectionStatusType;
import Objects.Player;
import Objects.Request;
import Objects.RequestType;
import Objects.Room;
import javafx.application.Platform;

public class Network implements INetworkMessageReceiverListener {

    private INetworkListener listener;
    private Socket socket;
    private PrintWriter printWriter;
    private NetworkMessageReceiver messageReceiver;
    private Queue<Request> requestsWaitingForResponse = new LinkedList<Request>();
    private ConnectionStatusType connectionStatusType = ConnectionStatusType.DISCONNECTED;
    private long timeLostConnection;

    public void setListener(INetworkListener listener) {
        this.listener = listener;
    }

    /**
     * closes connection to server
     */
    public void closeConnection() {
        if (printWriter != null)
            printWriter.close();

        if (messageReceiver != null)
            messageReceiver.stop();

        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Sends connect message to server
     * 
     * @param request
     */
    private void connectRequest(Request request) {
        String ipAddress = request.getStringParameter(Constants.IP_ADDRESS_PARAMETER);
        int port = request.getIntParameter(Constants.PORT_PARAMETER);
        Runnable runnable = () -> {
            int response = Constants.RESPONSE_SUCCESS;
            try {
                socket = new Socket();
                waitOnResponse(request);
                socket.connect(new InetSocketAddress(ipAddress, port), Constants.CONNECT_TIMEOUT);
                printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
                messageReceiver = new NetworkMessageReceiver(socket, this);

            } catch (UnknownHostException e) {
                response = Constants.RESPONSE_FAIL;
            } catch (IOException e) {
                response = Constants.RESPONSE_FAIL;
            }
            if (response == Constants.RESPONSE_FAIL) {
                requestsWaitingForResponse.poll();
                Platform.runLater(() -> listener.onServerResponse(request, Constants.RESPONSE_FAIL,
                        "Cannot establish connection"));
            }

        };
        Thread thread = new Thread(runnable);
        thread.setDaemon(true);
        thread.start();

    }

    /**
     * Sends request to server
     * 
     * @param requestType
     */
    public void sendRequest(RequestType requestType) {
        Request request = new Request(requestType);
        sendRequest(request);
    }

    /**
     * Sends request to server
     * 
     * @param request
     */
    public void sendRequest(Request request) {
        if (request.getRequestType() == RequestType.CONNECT) {
            connectRequest(request);
            return;
        }
        if (connectionStatusType != ConnectionStatusType.CONNECTED && request.getRequestType() != RequestType.PING) {
            System.out.println("Message skipped, connection is lost");
            return;
        }
        String requestMessage = request.getEncodedString();
        System.out.println("Sent message: " + requestMessage);
        printWriter.print(requestMessage);
        printWriter.flush();

        waitOnResponse(request);
    }

    /**
     * Waits on response from server If server did not send response calls listener
     * method that connection is lost After longer period of lost connection closes
     * the connection
     * 
     * @param request
     */
    private void waitOnResponse(Request request) {
        requestsWaitingForResponse.add(request);
        Runnable runnable = () -> {
            try {
                Thread.sleep(Constants.RESPONSE_TIMEOUT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (requestsWaitingForResponse.contains(request)) {
                requestsWaitingForResponse.remove(request);
                if (request.getRequestType() == RequestType.CONNECT) {
                    closeConnection();
                    Platform.runLater(() -> listener.onServerConnectionChanged(connectionStatusType));
                } else {
                    if (connectionStatusType == ConnectionStatusType.CONNECTED) {
                        Platform.runLater(() -> setConnection(ConnectionStatusType.PENDING));
                        timeLostConnection = System.currentTimeMillis();
                    } else if (System.currentTimeMillis() - timeLostConnection > Constants.DISCONNECT_INTERVAl) {
                        closeConnection();
                        timeLostConnection = 0;
                    }
                    System.out.println(
                            "Connection lost for: " + (System.currentTimeMillis() - timeLostConnection) / 1000.0 + "s");
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Sends periodically pings to server
     */
    private void sendPings() {
        Runnable runnable = () -> {
            try {
                Thread.sleep(Constants.PING_STANDARD_INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while (connectionStatusType != ConnectionStatusType.DISCONNECTED) {
                int interval = Constants.PING_STANDARD_INTERVAL;
                int pingType = Constants.PING_STANDARD;
                if (connectionStatusType == ConnectionStatusType.PENDING) {
                    interval = Constants.PING_LOST_CONNECTION_INTERVAL;
                    pingType = Constants.PING_LOST_CONNECTION;
                }

                Request ping = new Request(RequestType.PING);
                ping.addParameter(pingType, Constants.PING_TYPE);
                sendRequest(ping);

                try {
                    Thread.sleep(interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Set actual connection status type
     * 
     * @param connectionStatusType
     */
    private void setConnection(ConnectionStatusType connectionStatusType) {
        if (connectionStatusType == ConnectionStatusType.CONNECTED) {
            timeLostConnection = 0;
        }
        if (connectionStatusType != this.connectionStatusType) {
            this.connectionStatusType = connectionStatusType;
            listener.onServerConnectionChanged(connectionStatusType);
        }
    }

    @Override
    public void onResponseReceived(int responseCode, String message) {
        setConnection(ConnectionStatusType.CONNECTED);
        if (requestsWaitingForResponse.size() == 0) {
            listener.onServerMessageInvalid("Received response after timeout, network issues");
            return;
        }
        Request request = requestsWaitingForResponse.poll();
        if (request.getRequestType() != RequestType.PING) {
            if (request.getRequestType() == RequestType.CONNECT && responseCode == Constants.RESPONSE_SUCCESS) {
                sendPings();
            }
            listener.onServerResponse(request, responseCode, message);
        }
    }

    @Override
    public void onRoomListReceived(ArrayList<Room> rooms) {
        setConnection(ConnectionStatusType.CONNECTED);
        listener.onServerMessageRoomList(rooms);
    }

    @Override
    public void onStartGameReceived(int[] board) {
        setConnection(ConnectionStatusType.CONNECTED);
        listener.onServerMessageStartGame(board);
    }

    @Override
    public void onPlayersTurnReceived(String nick) {
        setConnection(ConnectionStatusType.CONNECTED);
        listener.onServerMessagePlayersTurn(nick);
    }

    @Override
    public void onRevealCardReceived(int cardId) {
        setConnection(ConnectionStatusType.CONNECTED);
        listener.onServerMessageRevealCard(cardId);
    }

    @Override
    public void onPlayerConnectionStatusReceived(ConnectionStatusType connectionStatus, String nick) {
        setConnection(ConnectionStatusType.CONNECTED);
        listener.onServerMessagePlayerConnectionStatus(connectionStatus, nick);
    }

    @Override
    public void onGameEndReceived() {
        setConnection(ConnectionStatusType.CONNECTED);
        listener.onServerMessageGameEnd();
    }

    @Override
    public void onGameStatusReceived(int[] board, String playerOnTurn, ArrayList<Player> players) {
        setConnection(ConnectionStatusType.CONNECTED);
        listener.onServerGameStatus(board, playerOnTurn, players);
    }

    @Override
    public void onInvalidMessageReceived(String message) {
        setConnection(ConnectionStatusType.CONNECTED);
        listener.onServerMessageInvalid(message);
    }

    @Override
    public void onConnectionClosed() {
        setConnection(ConnectionStatusType.DISCONNECTED);
    }

}
