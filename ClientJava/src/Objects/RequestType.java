package Objects;

/**
 * Request type to server
 */
public enum RequestType {
    CONNECT, LOGIN, REVEAL_CARD, ROOM_CREATE, ROOM_DELETE, ROOM_ENTER, ROOM_LEAVE, ROOM_START, PING

}
