package Objects;

/**
 * Application scenes
 */
public enum SceneType {
    CONNECT, LOBBY, ROOM, GAME, END_GAME
}
