package Objects;

/**
 * Class representing player
 */
public class Player implements Comparable<Player> {
    String nick;
    int score = 0;
    ConnectionStatusType connectionStatus = ConnectionStatusType.CONNECTED;

    public Player(String nick) {
        this.nick = nick;
    }

    public Player(String nick, int score, ConnectionStatusType connectionStatus) {
        this.nick = nick;
        this.score = score;
        this.connectionStatus = connectionStatus;
    }

    public String getNick() {
        return nick;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setConnectionStatus(ConnectionStatusType connectionStatus) {
        this.connectionStatus = connectionStatus;
    }

    public ConnectionStatusType getConnectionStatus() {
        return connectionStatus;
    }

    @Override
    public int compareTo(Player player2) {
        return Integer.compare(player2.getScore(), score);
    }

}
