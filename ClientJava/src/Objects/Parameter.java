package Objects;

/**
 * Parameter in protocol message
 */
public class Parameter {
    private String name;
    private int length;

    public Parameter(String name, int length) {
        this.name = name;
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public int getLength() {
        return length;
    }

}
