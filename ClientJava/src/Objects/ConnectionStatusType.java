package Objects;

/**
 * Client connection to server
 */
public enum ConnectionStatusType {
    CONNECTED, DISCONNECTED, PENDING
}
