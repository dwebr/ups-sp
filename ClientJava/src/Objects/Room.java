package Objects;

import java.util.ArrayList;

/**
 * Class representing room
 */
public class Room implements Comparable<Room> {
    int id;
    String owner;
    ArrayList<String> players;

    public Room(int id, String owner, ArrayList<String> players) {
        this.id = id;
        this.owner = owner;
        this.players = players;
    }

    public int getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public ArrayList<String> getPlayers() {
        return players;
    }

    public int getPlayersCount() {
        return players.size();
    }

    @Override
    public int compareTo(Room room2) {
        return Integer.compare(id, room2.getId());
    }

}
