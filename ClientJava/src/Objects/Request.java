package Objects;

import java.util.LinkedHashMap;

import Constants.Constants;
import Utils.Utils;

/**
 * Class representing request message to server
 */
public class Request {
    private RequestType requestType;
    private LinkedHashMap<String, ParameterValue> parameters;

    public Request(RequestType requestType) {
        this.requestType = requestType;
        parameters = new LinkedHashMap<String, ParameterValue>();
    }

    public void addParameter(int number, Parameter parameter) {
        parameters.put(parameter.getName(), new ParameterValue(number, parameter.getLength()));
    }

    public void addParameter(String text, Parameter parameter) {
        parameters.put(parameter.getName(), new ParameterValue(text, parameter.getLength()));
    }

    public String getStringParameter(Parameter parameter) {
        return parameters.get(parameter.getName()).getStringValue();
    }

    public int getIntParameter(Parameter parameter) {
        return parameters.get(parameter.getName()).getIntValue();
    }

    public String getEncodedString() {
        String requestString = Constants.MESSAGE_PROTOCOL_IDENTIFICATOR;
        requestString += getRequestLabel();

        StringBuilder requestBodyString = new StringBuilder();
        if (parameters != null) {
            parameters.values().forEach(parameter -> {
                requestBodyString.append(parameter.toString());
            });
        }

        requestString += Utils.getFormattedNumber(requestBodyString.length(), Constants.MESSAGE_LENGTH_DECIMALS);
        requestString += requestBodyString.toString();
        return requestString;
    }

    private String getRequestLabel() {
        switch (requestType) {
            case LOGIN:
                return Constants.REQUEST_LOGIN;
            case REVEAL_CARD:
                return Constants.REQUEST_REVEAL_CARD;
            case ROOM_ENTER:
                return Constants.REQUEST_ROOM_ENTER;
            case ROOM_CREATE:
                return Constants.REQUEST_ROOM_CREATE;
            case ROOM_DELETE:
                return Constants.REQUEST_ROOM_DELETE;
            case ROOM_LEAVE:
                return Constants.REQUEST_ROOM_LEAVE;
            case ROOM_START:
                return Constants.REQUEST_ROOM_START;
            case PING:
                return Constants.REQUEST_PING;
            default:
                return "";
        }
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public class ParameterValue {
        private String stringValue;
        private int intValue;
        private int lengthValue;
        boolean numerical;

        public ParameterValue(int number, int length) {
            numerical = true;
            intValue = number;
            lengthValue = length;
        }

        public ParameterValue(String text, int headerLength) {
            numerical = false;
            stringValue = text;
            lengthValue = headerLength;

        }

        public int getIntValue() {
            return intValue;
        }

        public String getStringValue() {
            return stringValue;
        }

        @Override
        public String toString() {
            if (numerical) {
                return Utils.getFormattedNumber(intValue, lengthValue);
            } else {
                return Utils.getFormattedNumber(stringValue.length(), lengthValue) + stringValue;
            }
        }
    }
}
